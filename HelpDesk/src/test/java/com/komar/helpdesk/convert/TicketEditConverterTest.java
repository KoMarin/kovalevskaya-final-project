package com.komar.helpdesk.convert;

import com.komar.helpdesk.dto.AttachmentPreviewDto;
import com.komar.helpdesk.dto.TicketForEditDto;
import com.komar.helpdesk.enums.States;
import com.komar.helpdesk.enums.Urgencies;
import com.komar.helpdesk.repository.model.Attachment;
import com.komar.helpdesk.repository.model.Category;
import com.komar.helpdesk.repository.model.Ticket;
import com.komar.helpdesk.repository.model.User;
import com.komar.helpdesk.service.CategoryService;
import com.komar.helpdesk.service.UserService;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import org.junit.Before;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author Маріна
 */
public class TicketEditConverterTest {

    private static final long ID = 1;
    private static final String NAME = "task";
    private static final LocalDate DESIRED_DATE = LocalDate.MAX;
    private static final Urgencies URGENCY = Urgencies.LOW;
    private static final States STATUS = States.APPROVED;
    private static final String DESCRIPTION = "bla bla bla";
    private static final long CATEGORY_ID = 1;
    private static final Category CATEGORY = Category.builder().id(CATEGORY_ID).name("category").build();
    private static final String OWNER_USERNAME = "test";
    private static final User OWNER = User.builder().email(OWNER_USERNAME).firstName("test").build();
    private static final String ATTACHMENT_NAME = "bla";
    private static final long ATTACHMENT_ID = 1;
    private static final Attachment ATTACHMENT = Attachment.builder().id(ATTACHMENT_ID).name(ATTACHMENT_NAME).build();
    private static final AttachmentPreviewDto ATTACHMENT_DTO = AttachmentPreviewDto.builder().id(ATTACHMENT_ID).name(ATTACHMENT_NAME).build();
    private static final List<Attachment> ATTACHMENTS = Arrays.asList(ATTACHMENT);
    private static final List<AttachmentPreviewDto> ATTACHMENTS_DTO = Arrays.asList(ATTACHMENT_DTO);
    
    private static final Ticket TICKET = Ticket.builder()
                .id(ID)
                .category(CATEGORY)
                .name(NAME)
                .description(DESCRIPTION)
                .urgency(URGENCY)
                .desiredDate(DESIRED_DATE)
                .status(STATUS)
                .owner(OWNER)
                .attachments(ATTACHMENTS)
                .build(); 
    
    private static final TicketForEditDto TICKET_DTO = TicketForEditDto.builder()
                .id(ID)
                .category(CATEGORY_ID)
                .name(NAME)
                .description(DESCRIPTION)
                .urgency(URGENCY)
                .desiredDate(DESIRED_DATE)
                .status(STATUS.getName())
                .owner(OWNER_USERNAME)
                .savedAttachments(ATTACHMENTS_DTO)
                .build(); 
            
    
    @InjectMocks
    private TicketEditConverter testingObject;
    
    @Mock 
    private CategoryService categoryService;
    
    @Mock 
    private UserService userService;
    
    @Mock
    private AttachmentConverter attachmentConverter;
    
    @Before
    public void setUpClass() {
        MockitoAnnotations.initMocks(this);
        when(attachmentConverter.toEntityList(ATTACHMENTS_DTO)).thenReturn(ATTACHMENTS);
        when(attachmentConverter.toPreviewDtoList(ATTACHMENTS)).thenReturn(ATTACHMENTS_DTO);
        when(userService.getUser(OWNER_USERNAME)).thenReturn(OWNER);
        when(categoryService.getCategory(CATEGORY_ID)).thenReturn(CATEGORY);

        System.out.println(attachmentConverter);
    }

    @Test
    public void testToEditDto() {
        System.out.println("toEditDto");
        TicketForEditDto result = testingObject.toEditDto(TICKET);
        System.out.println("mau");
        System.out.println(result);
        System.out.println(TICKET_DTO);
        assertEquals(TICKET_DTO, result);
    }

    @Test
    public void testToEntity() {
        System.out.println("toEntity");
        Ticket result = testingObject.toEntity(TICKET_DTO);
        assertEquals(TICKET, result);
    }
    
}
