package com.komar.helpdesk.service.impl;

import com.komar.helpdesk.convert.TicketEditConverter;
import com.komar.helpdesk.convert.TicketOverviewConverter;
import com.komar.helpdesk.convert.TicketPreviewConverter;
import com.komar.helpdesk.dto.AttachmentPreviewDto;
import com.komar.helpdesk.dto.TicketForEditDto;
import com.komar.helpdesk.dto.TicketOverviewDto;
import com.komar.helpdesk.dto.TicketPreviewDto;
import com.komar.helpdesk.enums.Actions;
import com.komar.helpdesk.enums.Roles;
import com.komar.helpdesk.enums.States;
import com.komar.helpdesk.enums.Urgencies;
import com.komar.helpdesk.repository.TicketRepository;
import com.komar.helpdesk.repository.model.Attachment;
import com.komar.helpdesk.repository.model.Category;
import com.komar.helpdesk.repository.model.Ticket;
import com.komar.helpdesk.repository.model.User;
import com.komar.helpdesk.service.EventService;
import com.komar.helpdesk.service.UserService;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.BeforeClass;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author Маріна
 */
public class TicketServiceImplTest {

    private static final long ID = 1;
    private static final String NAME = "task";
    private static final LocalDate DESIRED_DATE = LocalDate.MAX;
    private static final LocalDate CREATED_ON = LocalDate.now();
    private static final Urgencies URGENCY = Urgencies.LOW;
    private static final States STATUS = States.APPROVED;
    private static final String DESCRIPTION = "bla bla bla";
    
    private static final long CATEGORY_ID = 1;
    private static final Category CATEGORY = Category.builder().id(CATEGORY_ID).name("category").build();
    
    private static final String OWNER_USERNAME = "test";
    private static final User OWNER = User.builder()
            .email(OWNER_USERNAME)
            .firstName("test")
            .role(Roles.EMPLOYEE)
            .build();
    
    private static final String ATTACHMENT_NAME = "bla";
    private static final long ATTACHMENT_ID = 1;
    private static final Attachment ATTACHMENT = Attachment.builder().id(ATTACHMENT_ID).name(ATTACHMENT_NAME).build();
    private static final AttachmentPreviewDto ATTACHMENT_PREVIEW_DTO = AttachmentPreviewDto.builder().id(ATTACHMENT_ID).name(ATTACHMENT_NAME).build();
    private static final List<Attachment> ATTACHMENTS = Arrays.asList(ATTACHMENT);
    private static final List<AttachmentPreviewDto> ATTACHMENTS_PREVIEWS_DTO = Arrays.asList(ATTACHMENT_PREVIEW_DTO);
    
    private static final Ticket TICKET = Ticket.builder()
                .id(ID)
                .category(CATEGORY)
                .name(NAME)
                .description(DESCRIPTION)
                .urgency(URGENCY)
                .desiredDate(DESIRED_DATE)
                .status(STATUS)
                .owner(OWNER)
                .attachments(ATTACHMENTS)
                .createdOn(CREATED_ON)
                .build(); 
    
    private static final TicketForEditDto TICKET_FOR_EDIT_DTO = TicketForEditDto.builder()
                .id(ID)
                .category(CATEGORY_ID)
                .name(NAME)
                .description(DESCRIPTION)
                .urgency(URGENCY)
                .desiredDate(DESIRED_DATE)
                .status(STATUS.getName())
                .owner(OWNER_USERNAME)
                .savedAttachments(ATTACHMENTS_PREVIEWS_DTO)
                .build();
    
    private static final TicketPreviewDto TICKET_PREVIEW_DTO = TicketPreviewDto.builder()
                .id(ID)
                .name(NAME)
                .urgency(URGENCY)
                .desiredDate(DESIRED_DATE)
                .status(STATUS.getState())
                .my(true)
                .possibleActions(Arrays.asList(Actions.VIEW))
                .build();
    
    private static final TicketOverviewDto TICKET_OVERVIEW_DTO = TicketOverviewDto.builder()
                .name(NAME)
                .category(CATEGORY.getName())
                .urgency(URGENCY.getUrgency())
                .desiredDate(DESIRED_DATE)
                .status(STATUS.getState())
                .createdOn(CREATED_ON)
                .attachments(ATTACHMENTS_PREVIEWS_DTO)
                .history(new ArrayList<>())
                .comments(new ArrayList<>())
                .build();
    
    @InjectMocks
    private TicketServiceImpl testingObject;
    
    @Mock
    private TicketEditConverter editConverter;

    @Mock
    private UserService userService;

    @Mock
    private TicketRepository ticketRepository;

    @Spy
    private TicketPreviewConverter previewConverter;
    
    @Mock
    private TicketOverviewConverter overviewConverter;  

    @Mock
    private EventService eventService;
    
    @BeforeClass
    public static void setUpClass() {
       OWNER.setOwnedTickets(new HashSet<>(Arrays.asList(TICKET)));
       OWNER.setApprovedTickets(new HashSet<>());
       OWNER.setAssignedTickets(new HashSet<>());
    }
    
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        
        when(userService.getUser(OWNER_USERNAME)).thenReturn(OWNER);
        when(ticketRepository.getTicket(ID)).thenReturn(TICKET);
        
    }

    @Test
    public void testGetAllTickets() {
        System.out.println("getAllTickets");
        
        TICKET.setStatus(STATUS);
        
        Collection<TicketPreviewDto> result = testingObject.getAllTickets(OWNER_USERNAME);
        System.out.println(Arrays.asList(TICKET_PREVIEW_DTO));
        System.out.println(result);
        assertEquals(new HashSet(Arrays.asList(TICKET_PREVIEW_DTO)), result);
    }

    @Test
    public void testGetTicketForEdit() throws Exception {
        System.out.println("getTicketForEdit");
        when(editConverter.toEditDto(TICKET)).thenReturn(TICKET_FOR_EDIT_DTO);
        TICKET.setStatus(States.DRAFT);
        TicketForEditDto result = testingObject.getTicketForEdit(ID, OWNER_USERNAME);
        assertEquals(TICKET_FOR_EDIT_DTO, result);
    }

    @Test
    public void testGetTicketOverview() throws Exception {
        System.out.println("getTicketOverview");
        when(overviewConverter.toOverviewDto(TICKET)).thenReturn(TICKET_OVERVIEW_DTO);
        TicketOverviewDto result = testingObject.getTicketOverview(ID, OWNER_USERNAME);
        assertEquals(TICKET_OVERVIEW_DTO, result);
    }

    @Test
    public void testAdd() throws Exception {
        System.out.println("add");
        TICKET.setStatus(States.DRAFT);
        when(editConverter.toEntity(TICKET_FOR_EDIT_DTO)).thenReturn(TICKET);
        when(ticketRepository.update(TICKET)).thenReturn(TICKET);
        long result = testingObject.add(TICKET_FOR_EDIT_DTO, States.DRAFT.getName(), OWNER_USERNAME);
        verify(eventService).ticketEdited(TICKET, OWNER);
        assertEquals(ID, result);
    }
 
    @Test
    public void testEdit() throws Exception {
        System.out.println("edit");
        TICKET.setStatus(States.DRAFT);
        when(editConverter.toEntity(TICKET_FOR_EDIT_DTO)).thenReturn(TICKET);
        when(ticketRepository.update(TICKET)).thenReturn(TICKET);
        testingObject.edit(TICKET_FOR_EDIT_DTO, States.DRAFT.getName(), OWNER_USERNAME);
        verify(eventService).ticketEdited(TICKET, OWNER);
    }

    @Test
    public void testTransactTicketStatus() throws Exception {
        TICKET.setStatus(States.DRAFT);
        when(ticketRepository.update(TICKET)).thenReturn(TICKET);
        testingObject.transactTicketStatus(ID, Actions.SUBMIT.name(), OWNER_USERNAME);
        verify(eventService).ticketStatusChanged(TICKET, States.DRAFT, States.NEW, OWNER);
        assertEquals(States.NEW, TICKET.getStatus());
    }
    
}
