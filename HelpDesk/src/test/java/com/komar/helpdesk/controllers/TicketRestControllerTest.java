package com.komar.helpdesk.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.komar.helpdesk.dto.AttachmentPreviewDto;
import com.komar.helpdesk.dto.TicketForEditDto;
import com.komar.helpdesk.dto.TicketOverviewDto;
import com.komar.helpdesk.dto.TicketPreviewDto;
import com.komar.helpdesk.enums.Actions;
import com.komar.helpdesk.enums.Roles;
import com.komar.helpdesk.enums.States;
import com.komar.helpdesk.enums.Urgencies;
import com.komar.helpdesk.init.CORSFilter;
import com.komar.helpdesk.repository.model.Category;
import com.komar.helpdesk.service.CategoryService;
import com.komar.helpdesk.service.TicketService;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.securityContext;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 *
 * @author Маріна
 */

public class TicketRestControllerTest {private static final long ID = 1;
    private static final String NAME = "task";
    private static final LocalDate DESIRED_DATE = LocalDate.MAX;
    private static final Urgencies URGENCY = Urgencies.LOW;
    private static final States STATUS = States.DRAFT;
    private static final String DESCRIPTION = "bla bla bla";
    
    
    private static final LocalDate CREATED_ON = LocalDate.now();
    
    private static final long CATEGORY_ID = 1;
    private static final Category CATEGORY = Category.builder().id(CATEGORY_ID).name("category").build();
    private static final List<Category> CATEGORIES = Arrays.asList(CATEGORY);
    
    private static final String OWNER_USERNAME = "test";
    private static final String PASSWORD = "test";
    
    private static final String ATTACHMENT_NAME = "bla";
    private static final long ATTACHMENT_ID = 1;
    private static final AttachmentPreviewDto ATTACHMENT_PREVIEW_DTO = AttachmentPreviewDto.builder().id(ATTACHMENT_ID).name(ATTACHMENT_NAME).build();
    private static final List<AttachmentPreviewDto> ATTACHMENTS_PREVIEWS_DTO = Arrays.asList(ATTACHMENT_PREVIEW_DTO);

    
    private static final TicketForEditDto TICKET_FOR_EDIT_DTO = TicketForEditDto.builder()
                .id(ID)
                .category(CATEGORY_ID)
                .name(NAME)
                .description(DESCRIPTION)
                .urgency(URGENCY)
                .desiredDate(DESIRED_DATE)
                .status(STATUS.getName())
                .owner(OWNER_USERNAME)
                .savedAttachments(ATTACHMENTS_PREVIEWS_DTO)
                .build();
    
    
    private static final TicketPreviewDto TICKET_PREVIEW_DTO = TicketPreviewDto.builder()
                .id(ID)
                .name(NAME)
                .urgency(URGENCY)
                .desiredDate(DESIRED_DATE)
                .status(STATUS.getState())
                .my(true)
                .possibleActions(Arrays.asList(Actions.VIEW))
                .build();
      
    private static final TicketOverviewDto TICKET_OVERVIEW_DTO = TicketOverviewDto.builder()
                .name(NAME)
                .category(CATEGORY.getName())
                .urgency(URGENCY.getUrgency())
                .desiredDate(DESIRED_DATE)
                .status(STATUS.getState())
                .createdOn(CREATED_ON)
                .attachments(ATTACHMENTS_PREVIEWS_DTO)
                .history(new ArrayList<>())
                .comments(new ArrayList<>())
                .build();
    
    private MockMvc mockMvc;

    @Mock
    private CategoryService categoryService;

    @Mock
    private TicketService ticketService;

    @InjectMocks
    private TicketRestController ticketRestController;
    
    private Authentication authentication;
    
    @Before
    public void setUp() {
        System.out.println("set up");
        MockitoAnnotations.initMocks(this);
        
        mockMvc = MockMvcBuilders
                .standaloneSetup(ticketRestController)
                .apply(SecurityMockMvcConfigurers.springSecurity(new CORSFilter()))
                .build();
        
        authentication = new TestingAuthenticationToken(OWNER_USERNAME, PASSWORD, AuthorityUtils.createAuthorityList(Roles.EMPLOYEE.name()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
       
    }

    @Test
    public void testCreate_3args() throws Exception {
        System.out.println("create");

        when(ticketService.add(TICKET_FOR_EDIT_DTO, States.DRAFT.getName(), OWNER_USERNAME)).thenReturn(1L);
        
        
        ObjectMapper objectMapper = new ObjectMapper();
        String obj =  objectMapper.writeValueAsString(TICKET_FOR_EDIT_DTO);
        MockHttpServletRequestBuilder requestBuilder = 
                        post("/api/ticket")
                        .with(securityContext(SecurityContextHolder.getContext()))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(obj);
        
        mockMvc.perform(requestBuilder) 
                .andDo(print())
                .andExpect(authenticated().withUsername(OWNER_USERNAME))
                .andExpect(status().isOk())
                .andExpect(content().json("1"));
    }
    
    @Test
    public void testTickets() throws Exception {
        System.out.println("tickets");
        
        List<TicketPreviewDto> previews = new ArrayList<>(Arrays.asList(TICKET_PREVIEW_DTO));
        
        when(ticketService.getAllTickets(OWNER_USERNAME)).thenReturn(previews);
        
        MockHttpServletRequestBuilder requestBuilder = 
                        get("/api/ticket")
                        .with(securityContext(SecurityContextHolder.getContext()));
        
        mockMvc.perform(requestBuilder) 
                .andDo(print())
                .andExpect(authenticated().withUsername(OWNER_USERNAME))
                .andExpect(status().isOk())
                .andExpect(content().json("[{\"id\":1,"
                        + "\"name\":\"task\","
                        + "\"desiredDate\":\"31/12/+999999999\","
                        + "\"urgency\":\"Low\","
                        + "\"status\":\"Draft\","
                        + "\"my\":true,"
                        + "\"possibleActions\":[{\"resultState\":null,\"action\":\"Open\",\"name\":\"VIEW\"}]}]"));
    }

    @Test
    public void testGet() throws Exception {
        System.out.println("get");
        
        when(ticketService.getTicketOverview(ID, OWNER_USERNAME)).thenReturn(TICKET_OVERVIEW_DTO);
        
        MockHttpServletRequestBuilder requestBuilder = 
                        get("/api/ticket/" + ID)
                        .with(securityContext(SecurityContextHolder.getContext()));
        
        mockMvc.perform(requestBuilder) 
                .andDo(print())
                .andExpect(authenticated().withUsername(OWNER_USERNAME))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"name\":\"task\","
                        + "\"description\":null,"
                        + "\"createdOn\":[2018,2,26],"
                        + "\"desiredDate\":\"31/12/+999999999\","
                        + "\"assignee\":null,"
                        + "\"owner\":null,"
                        + "\"category\":\"category\","
                        + "\"urgency\":\"Low\","
                        + "\"status\":\"Draft\","
                        + "\"approver\":null,"
                        + "\"attachments\":[{\"id\":1,\"name\":\"bla\"}],"
                        + "\"comments\":[],"
                        + "\"history\":[],"
                        + "\"edit\":false,"
                        + "\"leaveFeedback\":false,"
                        + "\"viewFeedback\":false}"));
    }

    @Test
    public void testEdit_long_Authentication() throws Exception {
        System.out.println("edit");
        when(ticketService.getTicketForEdit(ID, OWNER_USERNAME)).thenReturn(TICKET_FOR_EDIT_DTO);
        when(categoryService.getAllCategories()).thenReturn(CATEGORIES);
        
        MockHttpServletRequestBuilder requestBuilder = 
                        get("/api/ticket/" + ID + "/edit")
                        .with(securityContext(SecurityContextHolder.getContext()));
        
        mockMvc.perform(requestBuilder) 
                .andDo(print())
                .andExpect(authenticated().withUsername(OWNER_USERNAME))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"ticket\":{\"id\":1,"
                        + "\"name\":\"task\","
                        + "\"description\":\"bla bla bla\","
                        + "\"desiredDate\":[999999999,12,31],"
                        + "\"category\":1,"
                        + "\"urgency\":\"Low\","
                        + "\"status\":\"DRAFT\","
                        + "\"owner\":\"test\","
                        + "\"attachments\":[],"
                        + "\"savedAttachments\":[{\"id\":1,\"name\":\"bla\"}],\"deletedAttachments\":[]}"
                        + ",\"categories\":[{\"id\":1,\"name\":\"category\"}]}"));
    }

    @Test
    public void testCreate_Authentication() throws Exception {
        System.out.println("create");
        when(categoryService.getAllCategories()).thenReturn(CATEGORIES);
        
        MockHttpServletRequestBuilder requestBuilder = 
                        get("/api/ticket/create")
                        .with(securityContext(SecurityContextHolder.getContext()));
        
        mockMvc.perform(requestBuilder) 
                .andDo(print())
                .andExpect(authenticated().withUsername(OWNER_USERNAME))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"ticket\":{\"id\":0,"
                        + "\"name\":null,"
                        + "\"description\":null,"
                        + "\"desiredDate\":null,"
                        + "\"category\":0,"
                        + "\"urgency\":null,"
                        + "\"status\":null,"
                        + "\"owner\":\"test\","
                        + "\"attachments\":[],"
                        + "\"savedAttachments\":[],"
                        + "\"deletedAttachments\":[]},"
                        + "\"categories\":[{\"id\":1,\"name\":\"category\"}]}"));
    }

    @Test
    public void testEdit_3args() throws Exception {
        System.out.println("edit");
        
        ObjectMapper objectMapper = new ObjectMapper();
        String obj =  objectMapper.writeValueAsString(TICKET_FOR_EDIT_DTO);
        
        MockHttpServletRequestBuilder requestBuilder = 
                        put("/api/ticket")
                        .with(securityContext(SecurityContextHolder.getContext()))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(obj);
        
        mockMvc.perform(requestBuilder) 
                .andDo(print())
                .andExpect(authenticated().withUsername(OWNER_USERNAME))
                .andExpect(status().isOk());
        
        verify(ticketService).edit(TICKET_FOR_EDIT_DTO, States.DRAFT.getName(), OWNER_USERNAME);
    }


    @Test
    public void testStatusTransaction() throws Exception {
        System.out.println("statusTransaction");
        
        ObjectMapper objectMapper = new ObjectMapper();
        String obj =  objectMapper.writeValueAsString(TICKET_FOR_EDIT_DTO);
        
        MockHttpServletRequestBuilder requestBuilder = 
                        post("/api/ticket/" + ID + "/" + States.NEW.name())
                        .with(securityContext(SecurityContextHolder.getContext()));
        
        mockMvc.perform(requestBuilder) 
                .andDo(print())
                .andExpect(authenticated().withUsername(OWNER_USERNAME))
                .andExpect(status().isOk());
        
        verify(ticketService).transactTicketStatus(ID, States.NEW.getName(), OWNER_USERNAME);
    }
    
}
