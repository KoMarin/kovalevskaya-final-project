package com.komar.helpdesk.controllers;

import com.komar.helpdesk.enums.Roles;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.junit.Assert.*;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 *
 * @author Маріна
 */
public class TicketControllerTest {

    private MockMvc mockMvc;
    private TicketController ticketController;
    
    @Before
    public void setUp() {
        ticketController = new TicketController();
        mockMvc = MockMvcBuilders
            .standaloneSetup(ticketController)
            .build();
    }

    @Test
    public void testList() throws Exception {
        System.out.println("list");
        mockMvc.perform(get("/ticket"))
            .andExpect(status().isOk())
            .andExpect(forwardedUrl("list"));
    }

    @Test
    public void testCreate() throws Exception {
        System.out.println("create");
        mockMvc.perform(get("/ticket/create"))
            .andExpect(status().isOk())
            .andExpect(forwardedUrl("edit"));
    }

    @Test
    public void testOverview() throws Exception {
        System.out.println("overview");
        mockMvc.perform(get("/ticket/1"))
            .andExpect(status().isOk())
            .andExpect(forwardedUrl("overview"));
    }
    
}
