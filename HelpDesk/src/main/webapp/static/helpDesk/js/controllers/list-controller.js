angular.module('ticketsApp').controller('ListController', ['$scope', 'TicketService', 'UrlService', function($scope, TicketService, UrlService) {
 
    $scope.sortType     = 'name';
    $scope.sortReverse  = false;  
    $scope.search = '';
    $scope.my = {my : ''};

    $scope.tickets;
    $scope.action = [];
    allTickets();

   
    $scope.drop = function(id) {
        console.info("Hiiii..... " + id);
        document.getElementById("myDropdown"+id).classList.toggle("show");
    };
    
    $scope.act = function (id){ 
        var action = $scope.action[id];
        console.info("ACTION " + action);
        
        switch (action.name){
            case 'VIEW_FEEDBACK': UrlService.redirectFeedack(id);  break;
            case 'ADD_FEEDBACK':  UrlService.redirectFeedack(id);  break;
            case 'VIEW':          UrlService.redirectOverview(id); break;
            case "EDIT":          UrlService.redirectEdit(id); break;
            
            default : TicketService.updateTicketStatus(id, action.name).then(
                        function(response){
                            console.info("Success");
                            allTickets();
                        },
                        function (errResponse){
                            if (errResponse.status === 403) UrlService.redirect403();
                            if (errResponse.status === 404) UrlService.redirect404();
                        }
                        ); 
                break;
        }
    };
    
    $scope.select = function(action, id) {
        console.info("SELECT: " + action + " for " + id);
        $scope.action[id] = action;
        document.getElementById("myDropdown"+id).classList.remove("show");
    };
    
    function urgencySort(ticket) {
        switch (ticket.urgency) {
            case 'CRITICAL':
                return 1;

            case 'HIGH':
                return 2;

            case 'AVERAGE':
                return 3;

            case 'LOW':
                return 4;
        }
    }
          
    $scope.sort = function (ticket){
        switch ($scope.sortType) {
            case 'id':
                return ticket.id;
            case 'name':
                return ticket.name;
            case 'date':
                return ticket.date;
            case 'urgency':
                return urgencySort(ticket);    
            case 'status':
                return ticket.status;  
        }
    };
      
    function allTickets(){
        console.info("GET ALL TICKETS");
            TicketService.getAllTickets().then(function(d) {
                console.info('SUCCESS: ');
                $scope.tickets = d;
                $scope.action = [];
            },
            function(errResponse){
                console.error('Error: ' + errResponse);
               if (errResponse.status === 403) UrlService.redirect403();
            }
        );
    } 
  
}]);


