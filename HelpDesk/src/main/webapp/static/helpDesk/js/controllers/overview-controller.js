angular.module('ticketsApp').controller('OverviewController', ['$scope', 'TicketService', 'UrlService', function($scope, TicketService, UrlService) {

    $scope.textPattern = "^[0-9a-zA-Z~.\\\"(),:;<>@\\[!#$%&'*+-/=?^_`{|} ]*";
    
    $scope.tableType = 'history';
    $scope.ticket = {};
    $scope.comment ={text: ""};
    
    $scope.id = UrlService.id();
    getOverview();
        
    function getOverview(){
       console.info("GET OVERVIEW");
           TicketService.getTicketOverview().then(function(d) {
               $scope.ticket = d;
               console.info("Success");
           },
           function(errResponse){
               console.error('Error: ' + errResponse);
               if (errResponse.status === 403) UrlService.redirect403();
               if (errResponse.status === 404) UrlService.redirect404();
           }
       );
    } 
        
    $scope.fullHistory = function(){
       console.info("GET FULL HISTORY");
           TicketService.getTicketHistory().then(function(d) {
               $scope.ticket.history = d;
               console.info("Success");
           },
           function(errResponse){
               console.error('Error: ' + errResponse);
               if (errResponse.status === 403) UrlService.redirect403();
               if (errResponse.status === 404) UrlService.redirect404();
           }
       );
    }; 
        
    $scope.fullComments = function(){
       console.info("GET ALL COMMENTS");
           TicketService.getTicketComments().then(function(comments) {
               console.info('SUCCESS');
               $scope.ticket.comments = comments;               
           },
           function(errResponse){
               console.error('Error while getting ticket overview: ' + errResponse);
               if (errResponse.status === 403) UrlService.redirect403();
               if (errResponse.status === 404) UrlService.redirect404();
           }
       );
    }; 
        
    $scope.addComment = function(){
       console.info("ADD COMMENT");
        TicketService.addComment($scope.comment.text).then(function() {
               console.info('SUCCESS');
               $scope.fullComments();
           },
           function(errResponse){
               console.error('Error: ' + errResponse);
               if (errResponse.status === 403) UrlService.redirect403();
               if (errResponse.status === 404) UrlService.redirect404();
           }
       );
    }; 

}]);


