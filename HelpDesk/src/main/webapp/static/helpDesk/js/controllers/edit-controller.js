angular.module('ticketsApp').controller('EditController', ['$scope', 'TicketService', 'UrlService', function($scope, TicketService, UrlService) {

    $scope.title = (UrlService.id() === 'create') ? 
        'Create new ticket' : 
        'Edit ticket (' + UrlService.id() + ')';

    $scope.namePattern = "^[0-9a-z~.\\\"(),:;<>@\\[!#$%&'*+-/=?^_`{|} ]*";
    $scope.textPattern = "^[0-9a-zA-Z~.\\\"(),:;<>@\\[!#$%&'*+-/=?^_`{|} ]*";

    $scope.edition = {};
    $scope.attachments = [];
    $scope.errors = [];
    $scope.comment = "";
    $scope.now = new Date();
    
    getEdition();
        
    function getEdition(){
    console.info("GET TICKET EDITION");
        TicketService.getTicketEdition().then(
            function(d) {
                $scope.edition = d;
                $scope.desiredDate  = new Date($scope.edition.ticket.desiredDate);
            },
            function(errResponse){
                console.error('Error while getting edition: ' + errResponse);
            }
        );
    }   
    $scope.save = function(state){       
        
        $scope.edition.ticket.status = state;
        $scope.edition.ticket.desiredDate = [$scope.desiredDate.getFullYear(), 
                                            $scope.desiredDate.getMonth() + 1, 
                                            $scope.desiredDate.getDate()];
                                        
        TicketService.saveTicket($scope.edition.ticket)
            .then(
                function (resp){
                    console.info("saved " + resp);
                    if ($scope.comment.length > "") 
                        TicketService.addComment($scope.comment, $scope.id);
                    if (state === "DRAFT")
                        UrlService.redirectOverview();
                    else
                        UrlService.redirectList();
                }
                , function (errResp){
                    console.error("ERROR");
               if (errResponse.status === 403) UrlService.redirect403();
               if (errResponse.status === 404) UrlService.redirect404();
                }
            );
        
    }; 
    
    $scope.browse = function (x){
        for (var i = 0; i < x.files.length; i++){
            var attachment = {name : "loading...", blob : "", loaded : false};
            $scope.attachments.push(x.files[i]);
            var id = $scope.edition.ticket.attachments.push(attachment) - 1;
            addFile(x.files[i], id);
            console.info('ATTACHMENT PUSHED: .... ' + x.files[i]);
        }
        $scope.$digest();
    };
    
    $scope.savedDelete = function(id){ 
        console.info("DELETE OLD " + id);
        $scope.edition.ticket.deletedAttachments.push($scope.edition.ticket.savedAttachments[id]);
        $scope.edition.ticket.savedAttachments.splice(id, 1);
    };
    
    $scope.newDelete = function(id){ 
        console.info("DELETE " + id);
        $scope.edition.ticket.attachments.splice(id, 1);
    };
    
    function addFile(file, id) {
        if ((file.name.split('.').pop() !== "pdf")
            &&(file.name.split('.').pop() !== "docx")
            &&(file.name.split('.').pop() !== "doc")
            &&(file.name.split('.').pop() !== "png")
            &&(file.name.split('.').pop() !== "jpeg")
            &&(file.name.split('.').pop() !== "jpg")){
        
            $scope.edition.ticket.attachments[id].name = 
                    "The selected file type is not allowed. "
                    + "Please select a file of one of the following types: pdf, png, doc, docx, jpg, jpeg.";
            
            return;
        }   
        
        if (file.size/1048576 > 5){
            
            $scope.edition.ticket.attachments[id].name = 
                    "The size of attached file should not be greater than 5 Mb. Please select another file.";

            return;
        }
        
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            $scope.edition.ticket.attachments[id].blob = reader.result;
            $scope.edition.ticket.attachments[id].loaded = true;
            $scope.edition.ticket.attachments[id].name = file.name;
           
            console.info($scope.edition.ticket.attachments[id].name + " loaded");
            $scope.$digest();
        };
        reader.onerror = function (error) {
          console.log('Error: ', error);
          $scope.edition.ticket.attachments[id].name = "ERROR";
        };
    }
        
}]);