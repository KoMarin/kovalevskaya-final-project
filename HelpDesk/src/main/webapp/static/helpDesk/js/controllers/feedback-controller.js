angular.module('ticketsApp').controller('FeedbackController', ['$scope', 'TicketService', 'UrlService', function($scope, TicketService, UrlService) {
    $scope.textPattern = "^[0-9a-zA-Z~.\\\"(),:;<>@\\[!#$%&'*+-/=?^_`{|} ]*";

    getfeedback();
    
    function getfeedback(){
       console.info("GET FEEDBACK");
           TicketService.getFeedback($scope.id).then(function(d) {
               console.info('edit d : ' + d);
               $scope.feedback = d;
               $scope.editable = ($scope.feedback.rate === 0);
               if ($scope.editable) $scope.feedback.rate = "";
               console.info("Success");
           },
           function(errResponse){
               console.error('Error while getting feedback: ' + errResponse);
               if (errResponse.status === 403) UrlService.redirect403();
               if (errResponse.status === 404) UrlService.redirect404();
           }
       );
    } 
    
    $scope.addFeedback = function(){
       console.info("ADD FEEDBACK");
           TicketService.addFeedback($scope.feedback).then(function(d) {
               console.info('edit d : ' + d);
               $scope.editable = false;
               UrlService.redirectList();
           },
           function(errResponse){
               console.error('Error: ' + errResponse);
               if (errResponse.status === 403) UrlService.redirect403();
               if (errResponse.status === 404) UrlService.redirect404();
           }
       );
    }; 
    
    $scope.back = function(){
        UrlService.redirectBack();
    };
    
}]);