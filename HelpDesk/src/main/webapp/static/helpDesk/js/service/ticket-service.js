angular.module('ticketsApp').factory('TicketService', ['$http', '$q', 'UrlService', function($http, $q, UrlService){
    
    var factory = {
        getAllTickets: getAllTickets,
        getTicketEdition: getTicketEdition,
        saveTicket: saveTicket,
        getTicketOverview: getTicketOverview,
        getTicketHistory: getTicketHistory,
        getTicketComments: getTicketComments,
        addComment:addComment,
        getFeedback: getFeedback,
        addFeedback: addFeedback, 
        updateTicketStatus: updateTicketStatus
    };

    return factory;

    function getAllTickets(){
        console.info('SERVICE Get all');
        var deferred = $q.defer();
        $http.get(UrlService.restUri).then(
        function (response) {
            deferred.resolve(response.data);
        },
        function(errResponse){
            deferred.reject(errResponse);
        });
        return deferred.promise;
    }
    
    function getTicketEdition(){
        console.info('SERVICE (get edition)');
        var deferred = $q.defer();
        $http.get(UrlService.restUriCurrent("edit")).then(
        function (response) {
            console.info('response : ' + response );
            deferred.resolve(response.data);
        },
        function(errResponse){
            console.error('Error while getting TicketEdition');
            deferred.reject(errResponse);
        });
        return deferred.promise;
    }
    
    function getTicketOverview(){
        console.info('SERVICE (overview)');
        var deferred = $q.defer();
        $http.get(UrlService.restUriCurrent()).then(
        function (response) {
            console.info('response : ' + response );
            deferred.resolve(response.data);
        },
        function(errResponse){
            console.error('Error while getting TicketOverview');
            deferred.reject(errResponse);
        });
        return deferred.promise;
    }
    
    function getFeedback(){
        console.info('SERVICE (get feedback)');
        var deferred = $q.defer();
        $http.get(UrlService.restUriCurrent("feedback")).then(
        function (response) {
            console.info('response : ' + response );
            deferred.resolve(response.data);
        },
        function(errResponse){
            console.error('Error while getting TicketOverview');
            deferred.reject(errResponse);
        });
        return deferred.promise;
    }
    
    function getTicketHistory(){
        console.info('SERVICE (history )');
        var deferred = $q.defer();
        $http.get(UrlService.restUriCurrent("history")).then(
        function (response) {
            console.info('response : ' + response );
            deferred.resolve(response.data);
        },
        function(errResponse){
            console.error('Error while getting TicketHistory');
            deferred.reject(errResponse);
        });
        return deferred.promise;
    }
    
    function getTicketComments(){
        console.info('SERVICE (comments )');
        var deferred = $q.defer();
        $http.get(UrlService.restUriCurrent("comment")).then(
        function (response) {
            console.info('response : ' + response );
            deferred.resolve(response.data);
        },
        function(errResponse){
            console.error('Error while getting TicketHistory');
            deferred.reject(errResponse);
        });
        return deferred.promise;
    }
    
    function addFeedback(feedback) {
        console.info("(SERVICE) feedback");
        var deferred = $q.defer();
        console.info("Inside " + deferred);
        $http.post(UrlService.restUriCurrent("feedback"), feedback)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function saveTicket(ticket){
        if (UrlService.id() !== "create") return updateTicket(ticket);
        else return createTicket(ticket);
    }
    
    function updateTicket(ticket) {
        console.info("SERVICE update ticket");
        var deferred = $q.defer();
        $http.put(UrlService.restUri, ticket)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while updating Ticket : ' +  errResponse.message);
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function createTicket(ticket) {
        console.info("SERVICE create ticket");
        var deferred = $q.defer();
        console.info("Inside " + deferred);
        $http.post(UrlService.restUri, ticket)
            .then(
            function (response) {
                console.info("Success; SET ID");
                UrlService.setId(response.data);
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while creating Ticket : ' +  errResponse.message);
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function updateTicketStatus(id, state) {
        console.info("SERVICE status");
        var deferred = $q.defer();
        $http.post(UrlService.restUriCurrent(state, id))
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function addComment(comment) {
        console.info("SERVICE add comment");
        var deferred = $q.defer();
        var commentDto = {text:comment};
        $http.post(UrlService.restUriCurrent("comment"), commentDto)
            .then(
            function (response) {
                console.info("Inside side");
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while updating Ticket : ' +  errResponse.message);
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
}]);

