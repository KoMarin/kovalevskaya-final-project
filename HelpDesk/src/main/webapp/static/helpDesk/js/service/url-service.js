angular.module('ticketsApp').factory('UrlService', [function(){
    
    var BASIC_URI = '/HelpDesk/ticket/';
    var REST_SERVICE_URI = '/HelpDesk/api/ticket/';
    
    var currentId = getCurrentId();
    function getCurrentId() {return currentId;};
    function setCurrentId(id) {currentId = id; console.info("ID RESETED: " + id);};
    
    var factory = {
        id: getCurrentId,
        setId: setCurrentId,
        restUriCurrent: restServiceUri,
        restUri: REST_SERVICE_URI,
        uri: uri,
        
        redirectOverview: redirectOverview,
        redirectList: redirectList,
        redirectBack: redirectBack,
        redirectFeedack: redirectFeedack,
        redirectEdit: redirectEdit,
        redirect403: redirect403,
        redirect404: redirect404
    };
    
    return factory;
    
    function restServiceUri(action, id){
        
        console.info("ID: "  + id);
        console.info("ACTION: "  + action);
        
        if (id === undefined) id = currentId;
        if (action === undefined) action = "";
        
        console.info("ID: "  + id);
        console.info("ACTION: "  + action);
        
        if (id !== "create")
            return  REST_SERVICE_URI + id + "/" + action;
        else return REST_SERVICE_URI + id;
    }
    
    function uri(id){
        if (id === undefined) id = currentId;
        return  BASIC_URI + id ;
    }
    
    function getCurrentId(){
        return location.href.split('/')[5];
    } 
    
    function redirect403(){
        window.location = '/HelpDesk/403';
    }
    
    function redirect404(){
        window.location = '/HelpDesk/404';
    }
    
    function redirectOverview(id){
        window.location = uri(id);
    }
    
    function redirectEdit(id){
        window.location = uri(id) + "/edit";
    }
    
    function redirectFeedack(id){
        window.location = uri(id) + "/feedback";
    }
    
    function redirectList(){
        window.location = BASIC_URI;
    }
    
    function redirectBack(){
        window.history.back();
    }
}]);


