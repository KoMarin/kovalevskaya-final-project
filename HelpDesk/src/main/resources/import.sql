Delete from User;                                                                                                                          
INSERT INTO User (id, first_name, last_name, position, phone, role, email, address, password)VALUES(1, 'User1','First', 'none','11001100','EMPLOYEE','user1_mogilev@yopmail.com','sreet 1, house 1, app 1', '$2a$10$em2Xw9hF0cp.xTTY5XOKZeKXosF0xuw9DrG2ErRZ1ofbUzJpLmGR6');
INSERT INTO User (id, first_name, last_name, position, phone, role, email, address, password)VALUES(2,'User2','Second','none','22002200','EMPLOYEE','user2_mogilev@yopmail.com','sreet 2, house 2, app 2', '$2a$10$em2Xw9hF0cp.xTTY5XOKZeKXosF0xuw9DrG2ErRZ1ofbUzJpLmGR6');
INSERT INTO User (id, first_name, last_name, position, phone, role, email, address, password)VALUES(3,'Manager1','Third','top manager', '33003300','MANAGER','manager1_mogilev@yopmail.com','sreet 3, house 3, app 3', '$2a$10$em2Xw9hF0cp.xTTY5XOKZeKXosF0xuw9DrG2ErRZ1ofbUzJpLmGR6');
INSERT INTO User (id, first_name, last_name, position, phone, role, email, address, password)VALUES(4,'Manager2','Fourth','just manager','4404400','MANAGER','manager2_mogilev@yopmail.com', 'sreet 4, house 4, app 4', '$2a$10$em2Xw9hF0cp.xTTY5XOKZeKXosF0xuw9DrG2ErRZ1ofbUzJpLmGR6');
INSERT INTO User (id, first_name, last_name, position, phone, role, email, address, password)VALUES(5,'Engineer1','Fifth','cheaf engineer','5505500','ENGINEER','engineer1_mogilev@yopmail.com','sreet 5, house 5, app 5', '$2a$10$em2Xw9hF0cp.xTTY5XOKZeKXosF0xuw9DrG2ErRZ1ofbUzJpLmGR6');
INSERT INTO User (id, first_name, last_name, position, phone, role, email, address, password)VALUES(6,'Engineer2','Sixth','just engineer','6606600','ENGINEER','engineer2_mogilev@yopmail.com','sreet 6, house 6, app 6', '$2a$10$em2Xw9hF0cp.xTTY5XOKZeKXosF0xuw9DrG2ErRZ1ofbUzJpLmGR6');

Delete from Category;
INSERT INTO Category (id, name) VALUES(1, 'Application & Services'),(2, 'Benefits & Paper Work'),(3, 'Hardware & Software'),(4, 'People Managment'),(5, 'Security & Access'),(6, 'Workplaces & Facilities');

Delete from Ticket;
INSERT INTO Ticket (id,name, description, created_on, desired_resolution_date, assignee_id, owner_id, status, category_id,  urgency, approver_id) VALUES (1,'task1','do it for me, please','2015-02-26','2015-03-26',null,1,'DRAFT',1,'LOW',null);
INSERT INTO Ticket (id,name, description, created_on, desired_resolution_date, assignee_id, owner_id, status, category_id,  urgency, approver_id) VALUES (2,'task2','do it for me, please','2016-04-30','2016-07-26',5,4,'IN_PROGRESS',2,'CRITICAL',null);
INSERT INTO Ticket (id,name, description, created_on, desired_resolution_date, assignee_id, owner_id, status, category_id,  urgency, approver_id) VALUES (3,'task3','do it for me, please','2017-05-19','2017-10-17',null,2,'NEW',3,'AVERAGE',3);
INSERT INTO Ticket (id,name, description, created_on, desired_resolution_date, assignee_id, owner_id, status, category_id,  urgency, approver_id) VALUES (4,'task4','do it for me, please','2017-05-19','2017-10-17',5,3,'HAS_FEEDBACK',2,'LOW',null);

Delete from Feedback;
INSERT INTO Feedback (id, user_id, rate, date, text, ticket_id)VALUES(1, 3, 5, '2017-10-16', 'It was so fast!', 4);

Delete from Comment;
INSERT INTO Comment (id, user_id, date, text, ticket_id)VALUES(1, 4, '2017-10-16', 'i am waiting it for a while! Can you be faster, please?!', 2);

Delete from History;
INSERT INTO History (id, ticket_id, date, action, user_id) VALUES(1, 1, '2015-02-26', 'created', 1);
INSERT INTO History (id, ticket_id, date, action, user_id) VALUES(2, 2, '2016-04-30', 'created', 4);
INSERT INTO History (id, ticket_id, date, action, user_id) VALUES(3, 3, '2017-05-19', 'created', 2);
INSERT INTO History (id, ticket_id, date, action, user_id) VALUES(4, 4, '2017-05-19', 'created', 3);
INSERT INTO History (id, ticket_id, date, action, user_id) VALUES(5, 4, '2017-10-16', 'feedback leaved', 3);
INSERT INTO History (id, ticket_id, date, action, user_id) VALUES(6, 2, '2017-10-16', 'commented', 4);


