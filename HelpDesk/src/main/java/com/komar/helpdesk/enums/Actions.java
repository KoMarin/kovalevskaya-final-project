package com.komar.helpdesk.enums;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.komar.helpdesk.exceptions.CannotExecuteActionExeption;
import static com.komar.helpdesk.enums.States.*;
import com.komar.helpdesk.repository.model.Ticket;
import com.komar.helpdesk.repository.model.User;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Маріна
 */

@JsonFormat(shape = JsonFormat.Shape.OBJECT) 
public enum Actions {
    
    EDIT("Edit") {
        
        @Override
        public boolean isAllowed(User user, Ticket ticket) {
            return (ticket.getStatus().equals(DRAFT_INVALID) || ticket.getStatus().equals(DRAFT))
                    && 
                    ticket.getOwner().equals(user);
        }

        @Override
        public void doIt(Ticket ticket, User user) throws CannotExecuteActionExeption {
            LOGGER.info("STATE: " + ticket.getStatus());
        }
    },
    
    SUBMIT(NEW, "Submit"){ 
       
        @Override
        public boolean isAllowed(User user, Ticket ticket) {
            return (ticket.getStatus().equals(DRAFT) || ticket.getStatus().equals(DECLINED))
                    &&(
                        user.getRole().equals(Roles.EMPLOYEE)
                        ||
                        user.getRole().equals(Roles.MANAGER) && ticket.getOwner().equals(user)
                    );
        }
    },

    APPROVE(APPROVED, "Approve"){
        
        @Override
        public boolean isAllowed(User user, Ticket ticket) {
            return ticket.getStatus().equals(NEW)
                    &&
                    user.getRole().equals(Roles.MANAGER)
                    &&
                    ticket.getOwner().getRole().equals(Roles.EMPLOYEE);
        }

        @Override
        public void doIt(Ticket ticket, User user) throws CannotExecuteActionExeption {
            super.doIt(ticket, user); 
            ticket.setApprover(user);
        }
    },
    
    DECLINE(DECLINED, "Decline"){
        
        @Override
        public boolean isAllowed(User user, Ticket ticket) {
            return ticket.getStatus().equals(NEW)
                    &&
                    user.getRole().equals(Roles.MANAGER)
                    &&
                    ticket.getOwner().getRole().equals(Roles.EMPLOYEE);
        }
    },
    CANCEL(CANCELED, "Cancel"){
        
        @Override
        public boolean isAllowed(User user, Ticket ticket) {
            return (
                        ticket.getStatus().equals(DRAFT)
                        &&(
                            user.getRole().equals(Roles.EMPLOYEE)
                            ||(
                            user.getRole().equals(Roles.MANAGER) && ticket.getOwner().equals(user)
                            )
                        )
                    )||(
                        ticket.getStatus().equals(NEW)
                        &&
                        user.getRole().equals(Roles.MANAGER)
                        &&
                        ticket.getOwner().getRole().equals(Roles.EMPLOYEE)
                    )||(
                        ticket.getStatus().equals(APPROVED)
                        &&
                        user.getRole().equals(Roles.ENGINEER)
                    );
        }
    },
    
    ASSIGN_TO_ME(States.IN_PROGRESS, "Assign to me"){
        
        @Override
        public boolean isAllowed(User user, Ticket ticket) {
            return ticket.getStatus().equals(APPROVED)
                    && 
                    user.getRole().equals(Roles.ENGINEER);
        }

        @Override
        public void doIt(Ticket ticket, User user) throws CannotExecuteActionExeption {
            super.doIt(ticket, user); 
            ticket.setAssignee(user);
        }
        
    },
    
    DONE(States.DONE, "Done"){
        
        @Override
        public boolean isAllowed(User user, Ticket ticket) {
            return ticket.getStatus().equals(IN_PROGRESS)
                    &&
                    user.getRole().equals(Roles.ENGINEER)
                    &&
                    ticket.getAssignee().equals(user);
        }
    },
    
    VIEW("Open"){
        
        @Override
        public boolean isAllowed(User user, Ticket ticket) {
            return ticket.getOwner().equals(user)
                    || (ticket.getAssignee() != null && ticket.getAssignee().equals(user))
                    || (ticket.getApprover()!= null && ticket.getApprover().equals(user))
                    ||(
                        user.getRole().equals(Roles.MANAGER)
                        &&
                        ticket.getStatus().equals(NEW)
                        &&
                        ticket.getOwner().getRole().equals(Roles.EMPLOYEE)
                    )   
                    ||(
                        user.getRole().equals(Roles.ENGINEER)
                        &&
                        ticket.getStatus().equals(APPROVED)
                    );   
        }
    },
    
    ADD_FEEDBACK(HAS_FEEDBACK, "Leave feedback"){

        @Override
        public void doIt(Ticket ticket, User user) throws CannotExecuteActionExeption {
            if (ticket.getFeedbacks().isEmpty()) {
                throw new CannotExecuteActionExeption(this, ticket, "For compliting this action ticket has to have feedback");
            }
            ticket.setStatus(HAS_FEEDBACK);
        }
        
        @Override
        public boolean isAllowed(User user, Ticket ticket) {
            return ticket.getStatus().equals(States.DONE)
                    &&
                    ticket.getOwner().equals(user);                  
        }
    },

    VIEW_FEEDBACK("Feedback"){
        @Override
        public boolean isAllowed(User user, Ticket ticket) {
            return ticket.getStatus().equals(States.HAS_FEEDBACK) 
                    &&
                    VIEW.isAllowed(user, ticket);
        }
    };
    
    private static  final Logger LOGGER = LogManager.getLogger(Actions.class);

    private States resultState; 
    private final String action;
    private final String name;

    private Actions(String action) {
        this.action = action;
        this.name = name();
    }

    private Actions(States state, String action){
        this(action);
        this.resultState = state;
    }

    public States getResultState() {
        return resultState;
    }

    public String getAction() {
        return action;
    }

    public String getName() {
        return name;
    }
    
    public abstract boolean isAllowed(User user, Ticket ticket);
    
    public static List<Actions> allowedActions(User user, Ticket ticket){
        return Arrays.asList(Actions.values())
                .stream()
                .filter(action -> action.isAllowed(user, ticket))
                .collect(Collectors.toList());
    }
    
    public void doIt(Ticket ticket, User user) throws CannotExecuteActionExeption{
        if (resultState == null) {
            LOGGER.warn("Action \'" + this.getAction() + "\' hasn't result state.");
            throw new CannotExecuteActionExeption(this);
        }
        ticket.setStatus(resultState);
    }

    
    
    @Override
    public String toString() {
        return "Actions{" + "resultState=" + resultState + ", action=" + action + ", name=" + name + '}';
    }
        
}
