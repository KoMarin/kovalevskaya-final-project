package com.komar.helpdesk.enums;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 *
 * @author Маріна
 */
public enum Urgencies {
    CRITICAL("Critical"), 
    HIGH("High"),
    AVERAGE("Medium"),
    LOW("Low");
    
    @JsonValue
    private final String urgency;

    private Urgencies(String urgency) {
        this.urgency = urgency;
    }

    public String getUrgency() {
        return urgency;
    }
  
}
