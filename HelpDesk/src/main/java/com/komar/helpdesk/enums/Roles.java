package com.komar.helpdesk.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;



public enum Roles {
    EMPLOYEE("User", new String[] {"Read", "Create"}),
    MANAGER("Manager",new String[] {"Read", "Create"}),
    ENGINEER("Engineer",new String[] {"Read"});
    
    private final List<GrantedAuthority> authorities;
    private final String role;
    
    private Roles(String role, String[] authoritiesString){
        this.role = role;
        authorities = new ArrayList<>();
        Arrays.stream(authoritiesString)
                .forEach(a -> authorities.add(new SimpleGrantedAuthority(a)));
    }

    public List<GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public String getRole() {
        return role;
    }
    
}