package com.komar.helpdesk.enums;

import com.komar.helpdesk.repository.model.Ticket;
import com.komar.helpdesk.repository.model.User;
import com.komar.helpdesk.service.UserService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Маріна
 */

public enum States {
    
    DRAFT_INVALID("Draft"){
        @Override
        public List<User> interestedUsers(Ticket ticket, UserService userService) {
            return new ArrayList<>();
        }         

        @Override
        public Actions getLedAction() {
            return Actions.EDIT;
        }
    },
    
    DRAFT("Draft"){
        @Override
        public List<User> interestedUsers(Ticket ticket, UserService userService) {
            return new ArrayList<>();
        }         

        @Override
        public Actions getLedAction() {
            return Actions.EDIT;
        } 
    },
    
    NEW("New"){

        @Override
        public List<User> interestedUsers(Ticket ticket, UserService userService) {
            return userService.getAllUsersWithRole(Roles.MANAGER);
        }        

        @Override
        public Actions getLedAction() {
            return Actions.SUBMIT;
        }

    },
    APPROVED("Approved"){
        
        @Override
        public List<User> interestedUsers(Ticket ticket, UserService userService) {
            List<User> users = userService.getAllUsersWithRole(Roles.ENGINEER);
            users.add(ticket.getOwner());
            return users;
        }        

        @Override
        public Actions getLedAction() {
            return Actions.APPROVE;
        }
        
    },
    DECLINED("Declined"){
        
        @Override
        public Actions getLedAction() {
            return Actions.DECLINE;
        }
    },
    
    IN_PROGRESS("In progress"){
        @Override
        public List<User> interestedUsers(Ticket ticket, UserService userService) {
            return new ArrayList<>();
        }            

        @Override
        public Actions getLedAction() {
            return Actions.ASSIGN_TO_ME;
        }    
    },
    
    DONE("Done"){        

        @Override
        public Actions getLedAction() {
            return Actions.DONE;
        }
    },
    
    CANCELED("Canceled"){
         @Override
        public List<User> interestedUsers(Ticket ticket, UserService userService) {
            if (ticket.getApprover() != null) {
                return Arrays.asList(ticket.getApprover(), ticket.getOwner());
            }
            return super.interestedUsers(ticket, userService);
        }        

        @Override
        public Actions getLedAction() {
            return Actions.CANCEL;
        }
    },
    
    HAS_FEEDBACK("Done"){
        @Override
        public List<User> interestedUsers(Ticket ticket, UserService userService) {
            return Arrays.asList(ticket.getAssignee());
        }        

        @Override
        public Actions getLedAction() {
            return Actions.ADD_FEEDBACK;
        }
        
    };
    
    private String state;

    private States(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public String getName() {
        return super.name();
    }

    public abstract Actions getLedAction();
    
    public List<User> interestedUsers(Ticket ticket, UserService userService){
        return Arrays.asList(ticket.getOwner());
    }
    
}
