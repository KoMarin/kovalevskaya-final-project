package com.komar.helpdesk.dto;

import com.komar.helpdesk.enums.Actions;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.komar.helpdesk.enums.Urgencies;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Маріна
 */
public class TicketPreviewDto {
    private long id;
    private String name;
    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy") 
    private LocalDate desiredDate;
    private Urgencies urgency;
    private String status;
    private boolean my;
    private List<Actions> possibleActions;

    public TicketPreviewDto() {
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public LocalDate getDesiredDate() {
        return desiredDate;
    }

    public Urgencies getUrgency() {
        return urgency;
    }

    public String getStatus() {
        return status;
    }

    public boolean isMy() {
        return my;
    }

    public List<Actions> getPossibleActions() {
        return possibleActions;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDesiredDate(LocalDate desiredDate) {
        this.desiredDate = desiredDate;
    }

    public void setUrgency(Urgencies urgency) {
        this.urgency = urgency;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setMy(boolean my) {
        this.my = my;
    }

    public void setPossibleActions(List<Actions> possibleStates) {
        this.possibleActions = possibleStates;
    }

    public static class Builder {
    
        private long id;
        private String name;
        private LocalDate desiredDate;
        private Urgencies urgency;
        private String status;
        private boolean my;
        private List<Actions> possibleActions;

        private Builder() {
        }

        public Builder id(final long value) {
            this.id = value;
            return this;
        }

        public Builder name(final String value) {
            this.name = value;
            return this;
        }

        public Builder desiredDate(final LocalDate value) {
            this.desiredDate = value;
            return this;
        }

        public Builder urgency(final Urgencies value) {
            this.urgency = value;
            return this;
        }

        public Builder status(final String value) {
            this.status = value;
            return this;
        }

        public Builder my(final boolean value) {
            this.my = value;
            return this;
        }

        public Builder possibleActions(final List<Actions> value) {
            this.possibleActions = value;
            return this;
        }

        public TicketPreviewDto build() {
            return new com.komar.helpdesk.dto.TicketPreviewDto(id, name, desiredDate, urgency, status, my, possibleActions);
        }
    }

    public static TicketPreviewDto.Builder builder() {
        return new TicketPreviewDto.Builder();
    }

    private TicketPreviewDto(final long id, final String name, final LocalDate desiredDate, final Urgencies urgency, final String status, final boolean my, final List<Actions> possibleStates) {
        this.id = id;
        this.name = name;
        this.desiredDate = desiredDate;
        this.urgency = urgency;
        this.status = status;
        this.my = my;
        this.possibleActions = possibleStates;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 11 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 11 * hash + Objects.hashCode(this.name);
        hash = 11 * hash + Objects.hashCode(this.desiredDate);
        hash = 11 * hash + Objects.hashCode(this.urgency);
        hash = 11 * hash + Objects.hashCode(this.status);
        hash = 11 * hash + (this.my ? 1 : 0);
        hash = 11 * hash + Objects.hashCode(this.possibleActions);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            System.out.println("null");
            return false;
        }
        if (getClass() != obj.getClass()) {
            System.out.println("class");
            return false;
        }
        final TicketPreviewDto other = (TicketPreviewDto) obj;
        if (this.id != other.id) {
            System.out.println("id");
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            System.out.println("name");
            return false;
        }
        if (!Objects.equals(this.desiredDate, other.desiredDate)) {
            System.out.println("desired");
            return false;
        }
        if (this.urgency != other.urgency) {
            System.out.println("urgency");
            return false;
        }
        if (!Objects.equals(this.status, other.status)) {
            System.out.println("status");
            return false;
        }
        if (this.my != other.my) {
            System.out.println("my");
            return false;
        }
        if (!Objects.equals(this.possibleActions, other.possibleActions)) {
            System.out.println("actions");
            return false;
        }
        System.out.println("everythings ok");
        return true;
    }

    @Override
    public String toString() {
        return "TicketPreviewDto{" + "id=" + id + ", name=" + name + ", desiredDate=" + desiredDate + ", urgency=" + urgency + ", status=" + status + ", my=" + my + ", possibleStates=" + possibleActions + '}';
    }
    
}
