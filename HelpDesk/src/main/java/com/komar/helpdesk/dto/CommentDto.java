package com.komar.helpdesk.dto;

import java.time.LocalDate;
import java.util.Objects;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 *
 * @author Маріна
 */
public class CommentDto {

    private String text;
    private String user;
    private LocalDate date;

    @Pattern(regexp = "^[0-9a-zA-Z~.\\\"(),:;<>@\\[!#$%&'*+-/=?^_`{|} ]*")
    @Size(min=1, max = 500)
    public CommentDto() {
    }

    public LocalDate getDate() {
        return date;
    }

    public String getText() {
        return text;
    }

    public String getUser() {
        return user;
    }      

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public static class Builder {

        private String text;
        private String user;
        private LocalDate date;

        private Builder() {
        }

        public Builder text(final String value) {
            this.text = value;
            return this;
        }

        public Builder user(final String value) {
            this.user = value;
            return this;
        }

        public Builder date(final LocalDate value) {
            this.date = value;
            return this;
        }

        public CommentDto build() {
            return new com.komar.helpdesk.dto.CommentDto(text, user, date);
        }
    }

    public static CommentDto.Builder builder() {
        return new CommentDto.Builder();
    }

    private CommentDto(final String text, final String user, final LocalDate date) {
        this.text = text;
        this.user = user;
        this.date = date;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.text);
        hash = 67 * hash + Objects.hashCode(this.user);
        hash = 67 * hash + Objects.hashCode(this.date);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CommentDto other = (CommentDto) obj;
        if (!Objects.equals(this.text, other.text)) {
            return false;
        }
        if (!Objects.equals(this.user, other.user)) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CommentDto{" + "text=" + text + ", user=" + user + ", date=" + date + '}';
    }

}
