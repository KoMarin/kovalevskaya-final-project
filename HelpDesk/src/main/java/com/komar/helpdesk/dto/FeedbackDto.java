package com.komar.helpdesk.dto;

import java.util.Objects;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 *
 * @author Маріна
 */
public class FeedbackDto {
    
    @Pattern(regexp = "^[0-9a-zA-Z~.\\\"(),:;<>@\\[!#$%&'*+-/=?^_`{|} ]*")
    private String text;

    @NotNull
    @Min(1) @Max(5)
    private int rate;
    
    private long ticketId;
    private String ticketName;

    public FeedbackDto() {
    }

    public FeedbackDto(long ticketId, String ticketName) {
        this.ticketId = ticketId;
        this.ticketName = ticketName;
    }
    
    public int getRate() {
        return rate;
    }
    
    public String getText() {
        return text;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getTicketId() {
        return ticketId;
    }

    public String getTicketName() {
        return ticketName;
    }

    public void setTicketId(long ticketId) {
        this.ticketId = ticketId;
    }

    public void setTicketName(String ticketName) {
        this.ticketName = ticketName;
    }

    public static class Builder {

        private String text;
        private int rate;
        private long ticketId;
        private String ticketName;

        private Builder() {
        }

        public Builder text(final String value) {
            this.text = value;
            return this;
        }

        public Builder rate(final int value) {
            this.rate = value;
            return this;
        }

        public Builder ticketId(final long value) {
            this.ticketId = value;
            return this;
        }

        public Builder ticketName(final String value) {
            this.ticketName = value;
            return this;
        }

        public FeedbackDto build() {
            return new com.komar.helpdesk.dto.FeedbackDto(text, rate, ticketId, ticketName);
        }
    }

    public static FeedbackDto.Builder builder() {
        return new FeedbackDto.Builder();
    }

    private FeedbackDto(final String text, final int rate, final long ticketId, final String ticketName) {
        this.text = text;
        this.rate = rate;
        this.ticketId = ticketId;
        this.ticketName = ticketName;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + Objects.hashCode(this.text);
        hash = 23 * hash + this.rate;
        hash = 23 * hash + (int) (this.ticketId ^ (this.ticketId >>> 32));
        hash = 23 * hash + Objects.hashCode(this.ticketName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FeedbackDto other = (FeedbackDto) obj;
        if (!Objects.equals(this.text, other.text)) {
            return false;
        }
        if (this.rate != other.rate) {
            return false;
        }
        if (this.ticketId != other.ticketId) {
            return false;
        }
        if (!Objects.equals(this.ticketName, other.ticketName)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "FeedbackDto{" + "text=" + text + ", rate=" + rate + ", ticketId=" + ticketId + ", ticketName=" + ticketName + '}';
    }

}
