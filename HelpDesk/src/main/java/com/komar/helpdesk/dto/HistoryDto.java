package com.komar.helpdesk.dto;

import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author Маріна
 */
public class HistoryDto {

    private LocalDate date;
    private String action;
    private String user;
    private String description;

    public HistoryDto() {
    }

    public String getAction() {
        return action;
    }

    public LocalDate getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public String getUser() {
        return user;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public static class Builder {

        private LocalDate date;
        private String action;
        private String user;
        private String description;

        private Builder() {
        }

        public Builder date(final LocalDate value) {
            this.date = value;
            return this;
        }

        public Builder action(final String value) {
            this.action = value;
            return this;
        }

        public Builder user(final String value) {
            this.user = value;
            return this;
        }

        public Builder description(final String value) {
            this.description = value;
            return this;
        }

        public HistoryDto build() {
            return new com.komar.helpdesk.dto.HistoryDto(date, action, user, description);
        }
    }

    public static HistoryDto.Builder builder() {
        return new HistoryDto.Builder();
    }

    private HistoryDto(final LocalDate date, final String action, final String user, final String description) {
        this.date = date;
        this.action = action;
        this.user = user;
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.date);
        hash = 59 * hash + Objects.hashCode(this.action);
        hash = 59 * hash + Objects.hashCode(this.user);
        hash = 59 * hash + Objects.hashCode(this.description);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HistoryDto other = (HistoryDto) obj;
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        if (!Objects.equals(this.action, other.action)) {
            return false;
        }
        if (!Objects.equals(this.user, other.user)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "HistoryDto{" + "date=" + date + ", action=" + action + ", user=" + user + ", description=" + description + '}';
    }
    
}
