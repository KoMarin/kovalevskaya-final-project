package com.komar.helpdesk.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Маріна
 */
public class AttachmentPreviewDto implements Serializable{
    private long id;
    private String name;

    public AttachmentPreviewDto() {
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 97 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AttachmentPreviewDto other = (AttachmentPreviewDto) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AttachmentPreviewDto{" + "id=" + id + ", name=" + name + '}';
    }

    public static class Builder {

        private long id;
        private String name;

        private Builder() {
        }

        public Builder id(final long value) {
            this.id = value;
            return this;
        }

        public Builder name(final String value) {
            this.name = value;
            return this;
        }

        public AttachmentPreviewDto build() {
            return new com.komar.helpdesk.dto.AttachmentPreviewDto(id, name);
        }
    }

    public static AttachmentPreviewDto.Builder builder() {
        return new AttachmentPreviewDto.Builder();
    }

    private AttachmentPreviewDto(final long id, final String name) {
        this.id = id;
        this.name = name;
    }
        
}
