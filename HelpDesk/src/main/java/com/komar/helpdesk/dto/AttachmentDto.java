package com.komar.helpdesk.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.komar.helpdesk.serialize.bytearray.ByteArrayDeserializer;
import java.io.Serializable;

/**
 *
 * @author Маріна
 */
public class AttachmentDto implements Serializable{

    @JsonDeserialize(using = ByteArrayDeserializer.class)
    
    private byte[] blob;

    private String name;
    private boolean loaded;

    public AttachmentDto() {
    }

    public byte[] getBlob() {
        return blob;
    }

    public void setBlob(byte[] blob) {
        this.blob = blob;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isLoaded() {
        return loaded;
    }

    public void setLoaded(boolean loaded) {
        this.loaded = loaded;
    }

    public static class Builder {

        private byte[] blob;
        private String name;
        private boolean loaded;

        private Builder() {
        }

        public Builder blob(final byte[] value) {
            this.blob = value;
            return this;
        }

        public Builder name(final String value) {
            this.name = value;
            return this;
        }

        public Builder loaded(final boolean value) {
            this.loaded = value;
            return this;
        }

        public AttachmentDto build() {
            return new com.komar.helpdesk.dto.AttachmentDto(blob, name, loaded);
        }
    }

    public static AttachmentDto.Builder builder() {
        return new AttachmentDto.Builder();
    }

    private AttachmentDto(final byte[] blob, final String name, final boolean loaded) {
        this.blob = blob;
        this.name = name;
        this.loaded = loaded;
    }
    
    @Override
    public String toString() {
        return "AttachmentDto{" + "name=" + name + ", blob=" + blob + '}';
    }
    
}
