package com.komar.helpdesk.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.komar.helpdesk.enums.Urgencies;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


/**
 *
 * @author Маріна
 */
public class TicketForEditDto implements Serializable{
    
    private long id;

    @NotNull
    @Pattern(regexp = "^[0-9a-z~.\\\"(),:;<>@\\[!#$%&'*+-/=?^_`{|} ]*")
    @Size(max = 100)
    private String name;
    
    @Pattern(regexp = "^[0-9a-zA-Z~.\\\"(),:;<>@\\[!#$%&'*+-/=?^_`{|} ]*")
    @Size(max = 500)
    private String description;

    @JsonDeserialize(using = LocalDateDeserializer.class)  
    @JsonSerialize(using = LocalDateSerializer.class)
    @FutureOrPresent()
    private LocalDate desiredDate;

    @NotNull
    private long category;
    
    @NotNull
    private Urgencies urgency;
    private String status;
    
    private String owner;

    private List<AttachmentDto> attachments;
    private List<AttachmentPreviewDto> savedAttachments;
    private List<AttachmentPreviewDto> deletedAttachments;
    
    public TicketForEditDto() {
        attachments = new ArrayList<>();
        deletedAttachments = new ArrayList<>();
        savedAttachments = new ArrayList<>();
    }
    
    public TicketForEditDto(String owner) {
        this();
        this.owner = owner;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDesiredDate() {
        return desiredDate;
    }

    public void setDesiredDate(LocalDate desiredDate) {
        this.desiredDate = desiredDate;
    }

    public long getCategory() {
        return category;
    }

    public void setCategory(long category) {
        this.category = category;
    }

    public Urgencies getUrgency() {
        return urgency;
    }

    public void setUrgency(Urgencies urgency) {
        this.urgency = urgency;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<AttachmentDto> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<AttachmentDto> attachments) {
        this.attachments = attachments;
    } 

    public List<AttachmentPreviewDto> getSavedAttachments() {
        return savedAttachments;
    }

    public void setSavedAttachments(List<AttachmentPreviewDto> savedAttachments) {
        this.savedAttachments = savedAttachments;
    }

    public List<AttachmentPreviewDto> getDeletedAttachments() {
        return deletedAttachments;
    }

    public void setDeletedAttachments(List<AttachmentPreviewDto> deletedAttachments) {
        this.deletedAttachments = deletedAttachments;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public static class Builder {

        private long id;
        private String name;
        private String description;
        private LocalDate desiredDate;
        private long category;
        private Urgencies urgency;
        private String status;
        private String owner;
        private List<AttachmentPreviewDto> savedAttachments;

        private Builder() {
        }

        public Builder id(final long value) {
            this.id = value;
            return this;
        }

        public Builder name(final String value) {
            this.name = value;
            return this;
        }

        public Builder description(final String value) {
            this.description = value;
            return this;
        }

        public Builder desiredDate(final LocalDate value) {
            this.desiredDate = value;
            return this;
        } 

        public Builder category(final long value) {
            this.category = value;
            return this;
        }

        public Builder urgency(final Urgencies value) {
            this.urgency = value;
            return this;
        }

        public Builder status(final String value) {
            this.status = value;
            return this;
        }

        public Builder owner(final String value) {
            this.owner = value;
            return this;
        }

        public Builder savedAttachments(final List<AttachmentPreviewDto> value) {
            this.savedAttachments = value;
            return this;
        }
     
        

        public TicketForEditDto build() {
            return new com.komar.helpdesk.dto.TicketForEditDto(id, name, description, desiredDate, category, urgency, status, savedAttachments, owner);
        }
    }

    public static TicketForEditDto.Builder builder() {
        return new TicketForEditDto.Builder();
    }

    private TicketForEditDto(final long id, final String name, final String description, final LocalDate desiredDate, final long category, final Urgencies urgency, final String status, final List<AttachmentPreviewDto> savedAttachments, String owner) {
        this(owner);
        this.id = id;
        this.name = name;
        this.description = description;
        this.desiredDate = desiredDate;
        this.category = category;
        this.urgency = urgency;
        this.status = status;
        this.savedAttachments = savedAttachments;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 79 * hash + Objects.hashCode(this.name);
        hash = 79 * hash + Objects.hashCode(this.description);
        hash = 79 * hash + Objects.hashCode(this.desiredDate);
        hash = 79 * hash + (int) (this.category ^ (this.category >>> 32));
        hash = 79 * hash + Objects.hashCode(this.urgency);
        hash = 79 * hash + Objects.hashCode(this.status);
        hash = 79 * hash + Objects.hashCode(this.owner);
        hash = 79 * hash + Objects.hashCode(this.attachments);
        hash = 79 * hash + Objects.hashCode(this.savedAttachments);
        hash = 79 * hash + Objects.hashCode(this.deletedAttachments);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TicketForEditDto other = (TicketForEditDto) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.desiredDate, other.desiredDate)) {
            return false;
        }
        if (this.category != other.category) {
            return false;
        }
        if (this.urgency != other.urgency) {
            return false;
        }
        if (!Objects.equals(this.status, other.status)) {
            return false;
        }
        if (!Objects.equals(this.owner, other.owner)) {
            return false;
        }
        if (!Objects.equals(this.attachments, other.attachments)) {
            return false;
        }
        if (!Objects.equals(this.savedAttachments, other.savedAttachments)) {
            return false;
        }
        if (!Objects.equals(this.deletedAttachments, other.deletedAttachments)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TicketForEditDto{" + "id=" + id + ", name=" + name + ", description=" + description + ", desiredDate=" + desiredDate + ", category=" + category + ", urgency=" + urgency + ", status=" + status + ", attachments=" + attachments + ", savedAttachments=" + savedAttachments + ", deletedAttachments=" + deletedAttachments + '}';
    }
    
}
