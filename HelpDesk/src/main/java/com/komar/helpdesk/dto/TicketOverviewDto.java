package com.komar.helpdesk.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Маріна
 */
public class TicketOverviewDto {

    private String name;
    private String description;

    @JsonDeserialize(using = LocalDateDeserializer.class)  
    @JsonSerialize(using = LocalDateSerializer.class)  
    private LocalDate createdOn;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy") 
    private LocalDate desiredDate;
    private String assignee;
    private String owner;
    private String category;
    private String urgency;
    private String status;
    private String approver;
    private List<AttachmentPreviewDto> attachments;
    private List<CommentDto> comments;
    private List<HistoryDto> history;
    private boolean edit;
    private boolean leaveFeedback;
    private boolean viewFeedback;

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCreatedOn(LocalDate createdOn) {
        this.createdOn = createdOn;
    }

    public void setDesiredDate(LocalDate desiredDate) {
        this.desiredDate = desiredDate;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setUrgency(String urgency) {
        this.urgency = urgency;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setApprover(String approver) {
        this.approver = approver;
    }

    public void setAttachments(List<AttachmentPreviewDto> attachments) {
        this.attachments = attachments;
    }

    public void setComments(List<CommentDto> comments) {
        this.comments = comments;
    }

    public void setHistory(List<HistoryDto> history) {
        this.history = history;
    }

    public void setEdit(boolean isMy) {
        this.edit = isMy;
    }

    public void setLeaveFeedback(boolean leaveFeedback) {
        this.leaveFeedback = leaveFeedback;
    }

    public void setViewFeedback(boolean hasFeedback) { 
        this.viewFeedback = hasFeedback;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public LocalDate getCreatedOn() {
        return createdOn;
    }

    public LocalDate getDesiredDate() {
        return desiredDate;
    }

    public String getAssignee() {
        return assignee;
    }

    public String getOwner() {
        return owner;
    }

    public String getCategory() {
        return category;
    }

    public String getUrgency() {
        return urgency;
    }

    public String getStatus() {
        return status;
    }

    public String getApprover() {
        return approver;
    }

    public List<AttachmentPreviewDto> getAttachments() {
        return attachments;
    }

    public List<CommentDto> getComments() {
        return comments;
    }

    public List<HistoryDto> getHistory() {
        return history;
    }

    public boolean isEdit() {
        return edit;
    }

    public boolean isLeaveFeedback() {
        return leaveFeedback;
    }

    public boolean isViewFeedback() {
        return viewFeedback;
    }

    public static class Builder {

        private String name;
        private String description;
        private LocalDate createdOn;
        private LocalDate desiredDate;
        private String assignee;
        private String owner;
        private String category;
        private String urgency;
        private String status;
        private String approver;
        private List<AttachmentPreviewDto> attachments;
        private List<CommentDto> comments;
        private List<HistoryDto> history;
        private boolean edit;
        private boolean leaveFeedback;
        private boolean viewFeedback;

        private Builder() {
        }

        public Builder name(final String value) {
            this.name = value;
            return this;
        }

        public Builder description(final String value) {
            this.description = value;
            return this;
        }

        public Builder createdOn(final LocalDate value) {
            this.createdOn = value;
            return this;
        }

        public Builder desiredDate(final LocalDate value) {
            this.desiredDate = value;
            return this;
        }

        public Builder assignee(final String value) {
            this.assignee = value;
            return this;
        }

        public Builder owner(final String value) {
            this.owner = value;
            return this;
        }

        public Builder category(final String value) {
            this.category = value;
            return this;
        }

        public Builder urgency(final String value) {
            this.urgency = value;
            return this;
        }

        public Builder status(final String value) {
            this.status = value;
            return this;
        }

        public Builder approver(final String value) {
            this.approver = value;
            return this;
        }

        public Builder attachments(final List<AttachmentPreviewDto> value) {
            this.attachments = value;
            return this;
        }

        public Builder comments(final List<CommentDto> value) {
            this.comments = value;
            return this;
        }

        public Builder history(final List<HistoryDto> value) {
            this.history = value;
            return this;
        }

        public Builder edit(final boolean value) {
            this.edit = value;
            return this;
        }

        public Builder leaveFeedback(final boolean value) {
            this.leaveFeedback = value;
            return this;
        }

        public Builder viewFeedback(final boolean value) {
            this.viewFeedback = value;
            return this;
        }

        public TicketOverviewDto build() {
            return new com.komar.helpdesk.dto.TicketOverviewDto(name, description, createdOn, desiredDate, assignee, owner, category, urgency, status, approver, attachments, comments, history, edit, leaveFeedback, viewFeedback);
        }
    }

    public static TicketOverviewDto.Builder builder() {
        return new TicketOverviewDto.Builder();
    }

    private TicketOverviewDto(final String name, final String description, final LocalDate createdOn, final LocalDate desiredDate, final String assignee, final String owner, final String category, final String urgency, final String status, final String approver, final List<AttachmentPreviewDto> attachments, final List<CommentDto> comments, final List<HistoryDto> history, final boolean isMy, final boolean leaveFeedback, final boolean hasFeedback) {
        this.name = name;
        this.description = description;
        this.createdOn = createdOn;
        this.desiredDate = desiredDate;
        this.assignee = assignee;
        this.owner = owner;
        this.category = category;
        this.urgency = urgency;
        this.status = status;
        this.approver = approver;
        this.attachments = attachments;
        this.comments = comments;
        this.history = history;
        this.edit = isMy;
        this.leaveFeedback = leaveFeedback;
        this.viewFeedback = hasFeedback;
    }

    

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.name);
        hash = 71 * hash + Objects.hashCode(this.description);
        hash = 71 * hash + Objects.hashCode(this.createdOn);
        hash = 71 * hash + Objects.hashCode(this.desiredDate);
        hash = 71 * hash + Objects.hashCode(this.assignee);
        hash = 71 * hash + Objects.hashCode(this.owner);
        hash = 71 * hash + Objects.hashCode(this.category);
        hash = 71 * hash + Objects.hashCode(this.urgency);
        hash = 71 * hash + Objects.hashCode(this.status);
        hash = 71 * hash + Objects.hashCode(this.approver);
        hash = 71 * hash + Objects.hashCode(this.attachments);
        hash = 71 * hash + Objects.hashCode(this.comments);
        hash = 71 * hash + Objects.hashCode(this.history);
        hash = 71 * hash + (this.edit ? 1 : 0);
        hash = 71 * hash + (this.viewFeedback ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TicketOverviewDto other = (TicketOverviewDto) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.createdOn, other.createdOn)) {
            return false;
        }
        if (!Objects.equals(this.desiredDate, other.desiredDate)) {
            return false;
        }
        if (!Objects.equals(this.assignee, other.assignee)) {
            return false;
        }
        if (!Objects.equals(this.owner, other.owner)) {
            return false;
        }
        if (!Objects.equals(this.category, other.category)) {
            return false;
        }
        if (!Objects.equals(this.urgency, other.urgency)) {
            return false;
        }
        if (!Objects.equals(this.status, other.status)) {
            return false;
        }
        if (!Objects.equals(this.approver, other.approver)) {
            return false;
        }
        if (!Objects.equals(this.attachments, other.attachments)) {
            return false;
        }
        if (!Objects.equals(this.comments, other.comments)) {
            return false;
        }
        if (!Objects.equals(this.history, other.history)) {
            return false;
        }
        if (this.edit != other.edit) {
            return false;
        }
        if (this.viewFeedback != other.viewFeedback) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TicketOverviewDto{" + "name=" + name + ", description=" + description + ", createdOn=" + createdOn + ", desiredDate=" + desiredDate + ", assignee=" + assignee + ", owner=" + owner + ", category=" + category + ", urgency=" + urgency + ", status=" + status + ", approver=" + approver + ", attachments=" + attachments + ", comments=" + comments + ", history=" + history + ", isMy=" + edit + ", hasFeedback=" + viewFeedback + '}';
    }
    
}
