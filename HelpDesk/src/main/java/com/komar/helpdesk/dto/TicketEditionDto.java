package com.komar.helpdesk.dto;

import com.komar.helpdesk.repository.model.Category;
import java.util.List;

/**
 *
 * @author Маріна
 */
public class TicketEditionDto {
    TicketForEditDto ticket;
    List<Category> categories;

    public TicketEditionDto(TicketForEditDto ticket, List<Category> categories) {
        this.ticket = ticket;  
        this.categories = categories;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public TicketForEditDto getTicket() {
        return ticket;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public void setTicket(TicketForEditDto ticket) {
        this.ticket = ticket;
    }
   
}
