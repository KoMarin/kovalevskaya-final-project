package com.komar.helpdesk.service.impl;

import com.komar.helpdesk.exceptions.AccessDeniedException;
import com.komar.helpdesk.convert.TicketEditConverter;
import com.komar.helpdesk.convert.TicketOverviewConverter;
import com.komar.helpdesk.convert.TicketPreviewConverter;
import com.komar.helpdesk.enums.Actions;
import com.komar.helpdesk.dto.TicketForEditDto;
import com.komar.helpdesk.dto.TicketOverviewDto;
import com.komar.helpdesk.dto.TicketPreviewDto;
import com.komar.helpdesk.exceptions.CannotExecuteActionExeption;
import com.komar.helpdesk.repository.TicketRepository;
import com.komar.helpdesk.enums.Roles;
import com.komar.helpdesk.enums.States;
import com.komar.helpdesk.exceptions.NotFoundException;
import com.komar.helpdesk.repository.model.Ticket;
import com.komar.helpdesk.repository.model.User;
import com.komar.helpdesk.service.AttachmentService;
import com.komar.helpdesk.service.EventService;
import com.komar.helpdesk.service.TicketService;
import com.komar.helpdesk.service.UserService;
import java.time.LocalDate;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Маріна
 */
@Service
public class TicketServiceImpl implements TicketService{
    
    private static  final Logger LOGGER = LogManager.getLogger(TicketServiceImpl.class);

    @Autowired 
    private UserService userService;
    
    @Autowired
    private AttachmentService attachmentService;
    
    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private TicketPreviewConverter previewConverter;
    
    @Autowired
    private TicketEditConverter editConverter;
    
    @Autowired
    private TicketOverviewConverter overviewConverter;  
    
    @Autowired
    private EventService eventService;    
    
    @Override
    public Collection<TicketPreviewDto> getAllTickets(long userId) {
        LOGGER.info("(SERVICE) Get all tickets " + userId);
        User user = userService.getUser(userId);
        return allTickets(user);
    } 

    @Override
    public Collection<TicketPreviewDto> getAllTickets(String username) {
        LOGGER.info("(SERVICE) Get all tickets " + username);
        User user = userService.getUser(username);
        return allTickets(user);
    }
    
    private Collection<TicketPreviewDto> allTickets(User user){
        LOGGER.info("(SERVICE) Get all tickets " + user);

        Set<TicketPreviewDto> tickets = new HashSet<>();
        
        tickets.addAll(previewConverter.toPreviewDtotList(user.getOwnedTickets(), user));
        tickets.addAll(previewConverter.toPreviewDtotList(user.getApprovedTickets(), user));
        tickets.addAll(previewConverter.toPreviewDtotList(user.getAssignedTickets(), user));
                
        tickets.stream().forEach(ticket -> ticket.setMy(true));
        
        if (user.getRole().equals(Roles.MANAGER)){
            tickets.addAll(previewConverter.toPreviewDtotList(ticketRepository.allEmployeeNewTickets(), user));
        }
        if (user.getRole().equals(Roles.ENGINEER)){
            tickets.addAll(previewConverter.toPreviewDtotList(ticketRepository.allApprovedTickets(), user));
        }
        
        LOGGER.info("(SERVICE) Return all Tickets");
        return tickets;
    }

    @Override
    public TicketForEditDto getTicketForEdit(long id, String username) throws AccessDeniedException, NotFoundException {
        LOGGER.info("(SERVICE) Get ticket for edit " + id);
        
        Ticket ticket = ticketRepository.getTicket(id);
        User user = userService.getUser(username);
        
        if (ticket == null) throw  new NotFoundException(id);
        
        if (Actions.EDIT.isAllowed(user, ticket)){
            TicketForEditDto ticketForEditDto = editConverter.toEditDto(ticket);
            LOGGER.info("(SERVICE) Return Ticket for edit");
            return ticketForEditDto;
        }
        LOGGER.info("(SERVICE) Editing this ticket is forbidden for current user");
        throw new AccessDeniedException(Actions.EDIT, ticket, user);
    }

    @Override
    public TicketOverviewDto getTicketOverview(long id, String username) throws AccessDeniedException, NotFoundException {
        LOGGER.info("(SERVICE) Get ticket overview " + id);
        
        Ticket ticket = ticketRepository.getTicket(id);
        User user = userService.getUser(username);
        
        if (ticket == null) throw  new NotFoundException(id);
        
        if (Actions.VIEW.isAllowed(user, ticket)){
            TicketOverviewDto ticketOverviewDto = overviewConverter.toOverviewDto(ticket);
            ticketOverviewDto.setEdit(Actions.EDIT.isAllowed(user, ticket));
            ticketOverviewDto.setLeaveFeedback(Actions.ADD_FEEDBACK.isAllowed(user, ticket));
            ticketOverviewDto.setViewFeedback(Actions.VIEW_FEEDBACK.isAllowed(user, ticket));
           
            LOGGER.info("(SERVICE) Return Ticket overview");
            return ticketOverviewDto;
        }
        LOGGER.info("(SERVICE) Viewing this ticket is forbidden for current user");
        throw new AccessDeniedException(Actions.VIEW, ticket, user);
    }

    @Override
    public long add(TicketForEditDto ticketDto, String expectedState, String username) throws AccessDeniedException {
        LOGGER.info("(SERVICE) Add ticket " + ticketDto);
        
        Ticket ticket = editConverter.toEntity(ticketDto);
        User user = userService.getUser(username);
        
        ticket.setOwner(user);
        
        Actions action = States.valueOf(expectedState).getLedAction();
        
        if (action.isAllowed(user, ticket)){
            try {
                action.doIt(ticket, user);
            } catch (CannotExecuteActionExeption ex) {
                LOGGER.warn("Cannot do " + action);
            }
            
            Ticket createdTicket = ticketRepository.update(ticket);
            eventService.ticketEdited(createdTicket, user);
            
            ticketDto.getAttachments().stream()
                    .filter(attachment -> attachment.isLoaded())
                    .forEach(attachment -> attachmentService.addAttachment(attachment, createdTicket, user));
            
            LOGGER.info("(SERVICE) Ticket is added");
            
            return createdTicket.getId();
        }
        else{
            LOGGER.info("(SERVICE) Adding this ticket is forbidden for current user");
            throw new AccessDeniedException(action, ticket, user);
        }
    }

    @Override
    public void edit(TicketForEditDto ticketDto, String expectedState, String username) throws AccessDeniedException {
        LOGGER.info("(SERVICE) Edit ticket " + ticketDto);
        System.out.println("" + editConverter);
        Ticket ticket = editConverter.toEntity(ticketDto);
        User user = userService.getUser(username);
        
        Actions action = States.valueOf(expectedState).getLedAction();
        
        if (action.isAllowed(user, ticket)){
            try {
                action.doIt(ticket, user);
            } catch (CannotExecuteActionExeption ex) {
                LOGGER.warn("Cannot do " + action);
            }
            
            ticket.setCreatedOn(LocalDate.now());
            
            Ticket updatedTicket = ticketRepository.update(ticket);
            eventService.ticketEdited(updatedTicket, user);
            
            ticketDto.getAttachments().stream()
                    .filter(attachment -> attachment.isLoaded())
                    .forEach(attachment -> attachmentService.addAttachment(attachment, updatedTicket, user));
            ticketDto.getDeletedAttachments().stream()
                    .forEach(attachment -> attachmentService.deleteAttachment(attachment, updatedTicket, user));
            
            LOGGER.info("(SERVICE) Ticket is aditd");
        }
        else{
            LOGGER.info("(SERVICE) Editing this ticket is forbidden for current user");
            throw new AccessDeniedException(action, ticket, user);
        }
    }

    @Override
    public void transactTicketStatus(long id, String actionString, String username) 
            throws AccessDeniedException, CannotExecuteActionExeption, NotFoundException {
        
        LOGGER.info("(SERVICE) Ticket status transact " + id + " -> " + actionString);
        
        Actions action = Actions.valueOf(actionString);
        
        Ticket ticket = ticketRepository.getTicket(id);
        User user = userService.getUser(username);

        if (ticket == null) throw  new NotFoundException(id);
        
        if (action.isAllowed(user, ticket)){
            System.out.println(ticket);
            States oldState = ticket.getStatus();
            System.out.println(ticket); 
            action.doIt(ticket, user);
            System.out.println(ticket);
            ticket = ticketRepository.update(ticket);
            System.out.println(ticket);
            eventService.ticketStatusChanged(ticket, oldState, ticket.getStatus(), user);
            LOGGER.info("(SERVICE) Ticket status transacted");
        }
        else {
            LOGGER.info("(SERVICE) Such status transaction is forbidden for current user");
            throw new AccessDeniedException(action, ticket, user);
        }
    }
    
}
