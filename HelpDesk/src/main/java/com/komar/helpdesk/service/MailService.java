package com.komar.helpdesk.service;

import com.komar.helpdesk.enums.Roles;
import com.komar.helpdesk.repository.model.Ticket;

/**
 *
 * @author Маріна
 */
public interface MailService {
    public void sendNotification(Ticket ticket, Roles doerRole);
}
