package com.komar.helpdesk.service;

import com.komar.helpdesk.dto.AttachmentDto;
import com.komar.helpdesk.dto.AttachmentPreviewDto;
import com.komar.helpdesk.dto.HistoryDto;
import com.komar.helpdesk.enums.States;
import com.komar.helpdesk.exceptions.AccessDeniedException;
import com.komar.helpdesk.exceptions.NotFoundException;
import com.komar.helpdesk.repository.model.Ticket;
import com.komar.helpdesk.repository.model.User;
import java.util.List;

/**
 *
 * @author Маріна
 */
public interface HistoryService {
    
    public List<HistoryDto> getHistory(long ticketId, String username) throws AccessDeniedException, NotFoundException ;
    
    public void ticketSave(Ticket ticket, String changed, User user);
    public void statusChanged(Ticket ticket, States oldState, States newStates, User user);
    public void fileAttached(AttachmentDto attachment, User user, Ticket ticket);
    public void fileRemoved(AttachmentPreviewDto attachment, User user, Ticket ticket);
}
