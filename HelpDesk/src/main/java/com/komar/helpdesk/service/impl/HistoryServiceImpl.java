package com.komar.helpdesk.service.impl;

import com.komar.helpdesk.convert.HistoryConverter;
import com.komar.helpdesk.dto.AttachmentDto;
import com.komar.helpdesk.dto.AttachmentPreviewDto;
import com.komar.helpdesk.dto.HistoryDto;
import com.komar.helpdesk.enums.Actions;
import com.komar.helpdesk.repository.HistoryRepository;
import com.komar.helpdesk.repository.model.History;
import com.komar.helpdesk.enums.States;
import com.komar.helpdesk.exceptions.AccessDeniedException;
import com.komar.helpdesk.exceptions.NotFoundException;
import com.komar.helpdesk.repository.TicketRepository;
import com.komar.helpdesk.repository.model.Ticket;
import com.komar.helpdesk.repository.model.User;
import com.komar.helpdesk.service.HistoryService;
import com.komar.helpdesk.service.UserService;
import java.time.LocalDate;
import java.util.List;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Маріна
 */

@Service
public class HistoryServiceImpl implements HistoryService{

    private static  final Logger LOGGER = LogManager.getLogger(HistoryServiceImpl.class);
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private TicketRepository ticketRepository;
    
    @Autowired
    private HistoryRepository historyRepository;
    
    @Autowired
    private HistoryConverter historyConverter;

    @Override
    public List<HistoryDto> getHistory(long ticketId, String username)  throws AccessDeniedException, NotFoundException {
        LOGGER.info("(SERVICE) Get History for " + ticketId);
        
        User user = userService.getUser(username);
        Ticket ticket = ticketRepository.getTicket(ticketId);
        
        if (ticket == null) throw  new NotFoundException(History.class, ticketId);
        
        if (Actions.VIEW.isAllowed(user, ticket)){
            LOGGER.info("(SERVICE) return history.");
            return historyConverter.toDtoList(ticket.getHistory());
        }
        LOGGER.info("(SERVICE) Getting history forbidden");
        throw new AccessDeniedException(Actions.VIEW, ticket, user);
    }

    @Override
    public void ticketSave(Ticket ticket, String changed, User user) {
        LOGGER.info("(SERVICE - HISTORY) Ticket Save");
        History history = History.builder()
                .ticket(ticket)
                .user(user)
                .date(LocalDate.now())
                .action("Ticket is " + changed)
                .description("Ticket is " + changed)
                .build();
        LOGGER.info("(SERVICE) Add history " + history);
        historyRepository.add(history);
    }

    @Override
    public void statusChanged(Ticket ticket, States oldState, States newState, User user) {
        LOGGER.info("(SERVICE - HISTORY) Ticket status changed");
        History history = History.builder()
                .ticket(ticket)
                .user(user)
                .date(LocalDate.now())
                .action("Ticket is changed")
                .description("Ticket is changed from " + oldState + " to " + newState)
                .build();
        LOGGER.info("(SERVICE) Add history " + history);
        historyRepository.add(history);
    }
    
    @Override
    public void fileAttached(AttachmentDto attachment, User user, Ticket ticket){
        LOGGER.info("(SERVICE - HISTORY) File attached");
        History history = History.builder()
                .user(user)
                .ticket(ticket)
                .date(LocalDate.now())
                .action("File is attached")
                .description("TFile is attached: " + attachment.getName())
                .build();
        LOGGER.info("(SERVICE) Add history " + history);
        historyRepository.add(history);
    }
    
    @Override
    public void fileRemoved(AttachmentPreviewDto attachment, User user, Ticket ticket){
        LOGGER.info("(SERVICE - HISTORY) File removed");
        History history = History.builder()
                .user(user)
                .ticket(ticket)
                .date(LocalDate.now())
                .action("File is removed")
                .description("TFile is removed: " + attachment.getName())
                .build();
        LOGGER.info("(SERVICE) Add history " + history);
        historyRepository.add(history);
    }

}
