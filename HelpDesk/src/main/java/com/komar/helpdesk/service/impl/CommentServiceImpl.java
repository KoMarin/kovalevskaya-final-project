package com.komar.helpdesk.service.impl;

import com.komar.helpdesk.convert.CommentConverter;
import com.komar.helpdesk.enums.Actions;
import com.komar.helpdesk.dto.CommentDto;
import com.komar.helpdesk.exceptions.AccessDeniedException;
import com.komar.helpdesk.exceptions.NotFoundException;
import com.komar.helpdesk.repository.CommentRepository;
import com.komar.helpdesk.repository.TicketRepository;
import com.komar.helpdesk.repository.model.Comment;
import com.komar.helpdesk.repository.model.Ticket;
import com.komar.helpdesk.repository.model.User;
import com.komar.helpdesk.service.CommentService;
import com.komar.helpdesk.service.UserService;
import java.time.LocalDate;  
import java.util.List;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Маріна
 */
@Service
public class CommentServiceImpl implements CommentService{
    
    private static  final Logger LOGGER = LogManager.getLogger(AttachmentServiceImpl.class);
    
    @Autowired
    private TicketRepository ticketRepository;
    
    @Autowired
    private CommentRepository commentRepository;
    
    @Autowired
    private CommentConverter commentConverter;
    
    @Autowired
    private UserService userService;

    @Override
    public List<CommentDto> getComments(long ticketId, String username) throws AccessDeniedException, NotFoundException {
        LOGGER.info("(SERVICE) Get comments for " + ticketId);
        
        User user = userService.getUser(username);
        Ticket ticket = ticketRepository.getTicket(ticketId);
        
        if (ticket == null) throw  new NotFoundException(Comment.class, ticketId);
        
        if (Actions.VIEW.isAllowed(user, ticket)){
            LOGGER.info("(SERVICE) return comments.");
            return commentConverter.toDtoList(ticket.getComments());
        }
        LOGGER.info("(SERVICE) Getting comments forbidden");
        throw new AccessDeniedException(Actions.VIEW, ticket, user);
    }

    @Override
    public long addComment(String username, String text, long ticketId) throws AccessDeniedException, NotFoundException {
        LOGGER.info("(SERVICE) Add comment " + text);
        Ticket ticket = ticketRepository.getTicket(ticketId);
        User user = userService.getUser(username);
        
        if (ticket == null) throw  new NotFoundException(Comment.class, ticketId);
        
        if (Actions.VIEW.isAllowed(user, ticket)){
            Comment commnent = Comment.builder()
                    .text(text)
                    .date(LocalDate.now())
                    .user(user)
                    .ticket(ticket)
                    .build();
            LOGGER.info("(SERVICE) Adding comment");
            return commentRepository.addComment(commnent);
        }
        LOGGER.info("(SERVICE) Adding comment forbidden");
        throw new AccessDeniedException(Actions.VIEW, ticket, user);
    }
    
}
