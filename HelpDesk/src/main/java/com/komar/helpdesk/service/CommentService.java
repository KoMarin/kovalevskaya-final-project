package com.komar.helpdesk.service;

import com.komar.helpdesk.dto.CommentDto;
import com.komar.helpdesk.exceptions.AccessDeniedException;
import com.komar.helpdesk.exceptions.NotFoundException;
import java.util.List;

/**
 *
 * @author Маріна
 */
public interface CommentService {
    public List<CommentDto> getComments(long ticketId, String username) throws AccessDeniedException, NotFoundException;
    public long addComment(String username, String text, long ticketId) throws AccessDeniedException , NotFoundException;
}
