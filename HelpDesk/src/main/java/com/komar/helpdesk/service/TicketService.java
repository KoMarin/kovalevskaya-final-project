package com.komar.helpdesk.service;

import com.komar.helpdesk.exceptions.AccessDeniedException;
import com.komar.helpdesk.dto.TicketForEditDto;
import com.komar.helpdesk.dto.TicketOverviewDto;
import com.komar.helpdesk.dto.TicketPreviewDto;
import com.komar.helpdesk.exceptions.CannotExecuteActionExeption;
import com.komar.helpdesk.exceptions.NotFoundException;
import java.util.Collection;

/**
 *
 * @author Маріна
 */

public interface TicketService {
    public Collection<TicketPreviewDto> getAllTickets(long userId);
    public Collection<TicketPreviewDto> getAllTickets(String username);
    public TicketForEditDto getTicketForEdit(long id, String username) throws AccessDeniedException, NotFoundException;
    public TicketOverviewDto getTicketOverview(long id, String username)throws AccessDeniedException, NotFoundException;
    public void transactTicketStatus(long id, String state, String username) throws AccessDeniedException, CannotExecuteActionExeption, NotFoundException;
    
    public long add(TicketForEditDto ticket, String expectedState, String username) throws AccessDeniedException;
    public void edit(TicketForEditDto ticket, String expectedState, String username) throws AccessDeniedException;
}
