package com.komar.helpdesk.service.impl;

import com.komar.helpdesk.repository.UserRepository;
import java.util.List;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.security.core.userdetails.User;

/**
 *
 * @author Маріна
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private static  final Logger LOGGER = LogManager.getLogger(UserDetailsServiceImpl.class);
    
    @Autowired
    private UserRepository userRepository;
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        LOGGER.info("(SERVICE - USERDETAILS) Get user details " + username);
        com.komar.helpdesk.repository.model.User user = userRepository.getUserByUserName(username);
        return buildUserForAuthentication(user, user.getRole().getAuthorities());
    }
    
    private User buildUserForAuthentication(com.komar.helpdesk.repository.model.User user,
            List<GrantedAuthority> authorities) {   
        User user1 = new User(user.getEmail(), user.getPassword(),
                    true, true, true, true, authorities);
        return user1;
    } 
}
