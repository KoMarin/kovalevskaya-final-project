package com.komar.helpdesk.service.impl;

import com.komar.helpdesk.repository.CategoryRepository;
import com.komar.helpdesk.repository.model.Category;
import com.komar.helpdesk.service.CategoryService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Маріна
 */

@Service
public class CategoryServiceImpl implements CategoryService{

    @Autowired
    private CategoryRepository categoryRepository;
    
    @Override
    public List<Category> getAllCategories() {
        return categoryRepository.allCategories();
    }

    @Override
    public Category getCategory(long id) {
        return categoryRepository.getCategory(id);
    }
    
}
