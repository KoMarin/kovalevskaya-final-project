package com.komar.helpdesk.service.impl;

import com.komar.helpdesk.enums.Roles;
import com.komar.helpdesk.enums.States;
import com.komar.helpdesk.repository.model.Ticket;
import com.komar.helpdesk.repository.model.User;
import com.komar.helpdesk.service.MailService;
import com.komar.helpdesk.service.UserService;
import java.text.MessageFormat;
import java.util.List;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/**
 *
 * @author Маріна
 */
@Service
@PropertySource("classpath:mail/emailconfig.properties")
@PropertySource("classpath:mail/MailMessages.properties")
public class MailServiceImpl implements MailService{

    private static  final Logger LOGGER = LogManager.getLogger(MailServiceImpl.class);
    
    private static final String TEMPLATE_NAME = "html/text";
    private static final String PROPERTY_FILE_NAME = "classpath:mail/MailMessages.properties";
    private static final String SUBJECT_START = "subject.";
    private static final String SUBJECT_DEFAULT_END = "standart";
    
    @Autowired
    Environment env;
    
    @Autowired
    private ApplicationContext applicationContext;
    
    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private TemplateEngine htmlTemplateEngine;
    
    @Autowired
    private UserService userService;
   
    @Override
    public void sendNotification(Ticket ticket, Roles doerRole) {
        
        LOGGER.info("(MAIL) Send hotification about " + ticket);
        
        List<User> recievers = ticket.getStatus().interestedUsers(ticket, userService);
        
        if (recievers.isEmpty()) {
            LOGGER.info("(MAIL) Nobody's interested in this event.");
            return;
        }
        
        String[] recieversEmails = getRecieversArray(recievers);
        String senderEmail = env.getProperty("mail.server.username");
        final String htmlContent = buildHtmlContent(ticket, doerRole, recievers);
        String subject = buildSubject(ticket.getStatus());
        
        Sender sender = new Sender(subject, 
                senderEmail,
                recieversEmails, 
                htmlContent, 
                mailSender);
        
        Thread sendThread = new Thread(sender);
        sendThread.start();
        
    }  
    
    private String buildSubject(States state){
        
        String propertyName = SUBJECT_START + state;
        String subject = env.getProperty(propertyName);
        
        if (subject == null) {
            subject = env.getProperty(SUBJECT_START + SUBJECT_DEFAULT_END);
        }

        subject = MessageFormat.format(subject, state.getState());
        return subject;
    }
    
    private String[] getRecieversArray(List<User> recievers){
        return recievers
                .stream()
                .map(r ->'<' + r.getEmail() + '>')
                .toArray(String[]::new);
    }
    
    private String buildHtmlContent(Ticket ticket, Roles doerRole, List<User> recievers){
        
        String reciever = (recievers.size() > 1)? 
                recievers.get(0).getRole().getRole() + "s" :
                recievers.get(0).getFirstName() + " " + recievers.get(0).getLastName();
        
        final Context context = new Context();
        
        context.setVariable("reciever", reciever);
        context.setVariable("state", ticket.getStatus());
        context.setVariable("role", doerRole.getRole());
        context.setVariable("id", ticket.getId());
        
        return this.htmlTemplateEngine.process(TEMPLATE_NAME, context);
    }
      
    private class Sender implements Runnable{

        private final String subject;
        private final String sender;
        private final String[] recievers;
        private final String htmlContent;
        private final JavaMailSender javaMailSender;

        public Sender(String subject, String sender, String[] recievers, String htmlContent, JavaMailSender javaMailSender) {
            this.subject = subject;
            this.sender = sender;
            this.recievers = recievers;
            this.htmlContent = htmlContent;
            this.javaMailSender = javaMailSender;
        }
        
        @Override
        public void run() {
            final MimeMessage mimeMessage = this.javaMailSender.createMimeMessage();
            final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, "UTF-8");
            
            try {
            
                message.setSubject(subject);
                message.setFrom(sender);
                message.setTo(recievers);
                message.setText(htmlContent, true);
                this.javaMailSender.send(mimeMessage);
            
            } catch (MessagingException ex) {
                LOGGER.error("SYSTEM FAILS ON SENDING MESSAGE: " + ex);
            }
        }
        
    }
    
}
