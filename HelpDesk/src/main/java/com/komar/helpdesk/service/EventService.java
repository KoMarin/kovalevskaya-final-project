package com.komar.helpdesk.service;

import com.komar.helpdesk.dto.AttachmentDto;
import com.komar.helpdesk.dto.AttachmentPreviewDto;
import com.komar.helpdesk.enums.States;
import com.komar.helpdesk.repository.model.Ticket;
import com.komar.helpdesk.repository.model.User;

/**
 *
 * @author Маріна
 */
public interface EventService {
    public void ticketEdited(Ticket ticket, User user);
    public void ticketCreated(Ticket ticket, User user);
    public void ticketStatusChanged(Ticket ticket, States oldState, States newState, User user);
    public void feedbackProvided(Ticket ticket);
    public void fileAttached(AttachmentDto attachment, User user, Ticket ticket);
    public void fileRemoved(AttachmentPreviewDto attachment, User user, Ticket ticket);
}
