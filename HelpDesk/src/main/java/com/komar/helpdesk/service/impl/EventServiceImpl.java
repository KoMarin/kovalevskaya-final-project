package com.komar.helpdesk.service.impl;

import com.komar.helpdesk.dto.AttachmentDto;
import com.komar.helpdesk.dto.AttachmentPreviewDto;
import com.komar.helpdesk.enums.States;
import com.komar.helpdesk.repository.model.Ticket;
import com.komar.helpdesk.repository.model.User;
import com.komar.helpdesk.service.EventService;
import com.komar.helpdesk.service.HistoryService;
import com.komar.helpdesk.service.MailService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Маріна
 */
@Service
public class EventServiceImpl implements EventService{

    private static  final Logger LOGGER = LogManager.getLogger(EventServiceImpl.class);
    
    @Autowired
    private HistoryService historyService;

    @Autowired
    private MailService mailService;
    
    @Override
    public void ticketEdited(Ticket ticket, User user) {
        LOGGER.info("(EVENT : ticket edited) Save to History");
        historyService.ticketSave(ticket, "edited", user);
        if (ticket.getStatus().equals(States.NEW)) {
            LOGGER.info("edited ticket also has state \'NEW\' - one more event");
            ticketStatusChanged(ticket, States.DRAFT, States.NEW, user);
        }
    }

    @Override
    public void ticketCreated(Ticket ticket, User user) {
        LOGGER.info("(EVENT : ticket created) Save to History");
        historyService.ticketSave(ticket, "created", user);
        if (ticket.getStatus().equals(States.NEW)) {
            LOGGER.info("created ticket also has state \'NEW\' - one more event");
            ticketStatusChanged(ticket, States.DRAFT, States.NEW, user);
        }
    }

    @Override
    public void ticketStatusChanged(Ticket ticket, States oldState, States newState, User user) {
        LOGGER.info("(EVENT : ticket status changed) Save to History, Send email notifications");
        historyService.statusChanged(ticket, oldState, newState, user);
        mailService.sendNotification(ticket, user.getRole());
    }

    @Override
    public void feedbackProvided(Ticket ticket) {
        LOGGER.info("(EVENT : feedback provided) Send email notifications");
        mailService.sendNotification(ticket, ticket.getOwner().getRole());
    }

    @Override
    public void fileAttached(AttachmentDto attachment, User user, Ticket ticket) {
        LOGGER.info("(EVENT : file attached) Save to History");
        historyService.fileAttached(attachment, user, ticket);
    }

    @Override
    public void fileRemoved(AttachmentPreviewDto attachment, User user, Ticket ticket) {
        LOGGER.info("(EVENT : file removed) Save to History");
        historyService.fileRemoved(attachment, user, ticket);
    }
    
}
