package com.komar.helpdesk.service;

import com.komar.helpdesk.enums.Roles;
import com.komar.helpdesk.repository.model.User;
import java.util.List;

/**
 *
 * @author Маріна
 */
public interface UserService {
    
    public User getUser(long id);
    public User getUser(String username);
    public List<User> getAllUsersWithRole(Roles role); 
    
}
