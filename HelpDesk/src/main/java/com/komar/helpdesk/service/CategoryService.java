package com.komar.helpdesk.service;

import com.komar.helpdesk.repository.model.Category;
import java.util.List;

/**
 *
 * @author Маріна
 */
public interface CategoryService {
    public List<Category> getAllCategories();
    public Category getCategory(long id);
}
