package com.komar.helpdesk.service;

import com.komar.helpdesk.exceptions.AccessDeniedException;
import com.komar.helpdesk.dto.AttachmentDto;
import com.komar.helpdesk.dto.AttachmentPreviewDto;
import com.komar.helpdesk.exceptions.NotFoundException;
import com.komar.helpdesk.repository.model.Ticket;
import com.komar.helpdesk.repository.model.User;

/**
 *
 * @author Маріна
 */
public interface AttachmentService {
    public AttachmentDto getAttachment(long id, String username)throws AccessDeniedException, NotFoundException;
    public void deleteAttachment(AttachmentPreviewDto attachmentPreviewDto, Ticket ticket, User user);
    public long addAttachment(AttachmentDto attachment, Ticket ticket, User user);
}
