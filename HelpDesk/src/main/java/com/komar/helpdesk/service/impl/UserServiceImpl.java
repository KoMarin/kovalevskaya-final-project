package com.komar.helpdesk.service.impl;

import com.komar.helpdesk.repository.UserRepository;
import com.komar.helpdesk.enums.Roles;
import com.komar.helpdesk.repository.model.User;
import com.komar.helpdesk.service.UserService;
import java.util.List;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Маріна
 */
@Service
public class UserServiceImpl implements UserService{

    private static  final Logger LOGGER = LogManager.getLogger(UserServiceImpl.class);
    
    @Autowired
    private UserRepository repository;
    
    @Override
    public User getUser(long id) {
        LOGGER.info("(SERVICE) Get User " + id);
        return repository.getUser(id);
    }

    @Override
    public User getUser(String username) {
        return repository.getUserByUserName(username);
    }

    @Override
    public List<User> getAllUsersWithRole(Roles role) {
        LOGGER.info("(SERVICE) Get User " + role);
        return repository.getUsersWithRole(role);
    }
    
}
