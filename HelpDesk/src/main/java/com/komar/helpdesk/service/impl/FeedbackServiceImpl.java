package com.komar.helpdesk.service.impl;

import com.komar.helpdesk.convert.FeedbackConverter;
import com.komar.helpdesk.enums.Actions;
import com.komar.helpdesk.dto.FeedbackDto;
import com.komar.helpdesk.exceptions.AccessDeniedException;
import com.komar.helpdesk.exceptions.NotFoundException;
import com.komar.helpdesk.repository.FeedbackRepository;
import com.komar.helpdesk.repository.TicketRepository;
import com.komar.helpdesk.repository.model.Comment;
import com.komar.helpdesk.repository.model.Feedback;
import com.komar.helpdesk.repository.model.Ticket;
import com.komar.helpdesk.repository.model.User;
import com.komar.helpdesk.service.EventService;
import com.komar.helpdesk.service.FeedbackService;
import com.komar.helpdesk.service.UserService;
import org.apache.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Маріна
 */

@Service
public class FeedbackServiceImpl implements FeedbackService{

    private static  final org.apache.log4j.Logger LOGGER = LogManager.getLogger(FeedbackServiceImpl.class);
    
    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private EventService eventService;
    
    @Autowired
    private FeedbackConverter feedbackConverter;
    
    @Autowired
    private FeedbackRepository feedbackRepository;
    
    @Override
    public FeedbackDto getFeedback(long ticketId, String username) throws AccessDeniedException, NotFoundException {
        LOGGER.info("(SERVICE) Get feedback " + ticketId);
        Ticket ticket = ticketRepository.getTicket(ticketId);
        
        if (ticket == null) throw  new NotFoundException(Feedback.class, ticketId);
        
        User user = userService.getUser(username);
        
        if (Actions.VIEW_FEEDBACK.isAllowed(user, ticket)) {
            LOGGER.info("(SERVICE) This ticket already has feedback and current user is allowed to see it.");
            LOGGER.info("(SERVICE) Return feedback.");
            return feedbackConverter.toDto(ticket.getFeedbacks().get(0));
        }
        
        if (Actions.ADD_FEEDBACK.isAllowed(user, ticket)) {
            LOGGER.info("(SERVICE) This ticket hasn't feedback yet and current user is allowed to provide it.");
            LOGGER.info("(SERVICE) Return empty feedback.");
            return new FeedbackDto(ticketId, ticket.getName());
        }
        
        LOGGER.info("(SERVICE) Current user isn't allowed to see the feedback.");
        throw new AccessDeniedException(Actions.VIEW_FEEDBACK, ticket, user);
    }
  
    @Override
    public long addFeedback(FeedbackDto feedbackDto, String username) throws AccessDeniedException, NotFoundException {
        LOGGER.info("(SERVICE) Add feedback " + feedbackDto);
        
        Ticket ticket = ticketRepository.getTicket(feedbackDto.getTicketId());
        User user = userService.getUser(username);
        
        if (ticket == null) throw  new NotFoundException(Feedback.class, ticket.getId());
        
        if (Actions.ADD_FEEDBACK.isAllowed(user, ticket)) {
            Feedback feedback = feedbackConverter.toEntity(feedbackDto);
            feedback.setTicket(ticket);
            feedback.setUser(user);
            feedback = feedbackRepository.add(feedback);
            ticket = feedback.getTicket();
            eventService.feedbackProvided(ticket);
            LOGGER.info("(SERVICE) Feedback provided");
            return feedback.getId();
        }
        LOGGER.info("(SERVICE) Prividin feedback forbidden for current user.");
        throw new AccessDeniedException();
    }
    
}
