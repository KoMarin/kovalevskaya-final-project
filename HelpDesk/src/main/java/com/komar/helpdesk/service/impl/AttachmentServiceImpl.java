package com.komar.helpdesk.service.impl;

import com.komar.helpdesk.exceptions.AccessDeniedException;
import com.komar.helpdesk.convert.AttachmentConverter;
import com.komar.helpdesk.enums.Actions;
import com.komar.helpdesk.dto.AttachmentDto;
import com.komar.helpdesk.dto.AttachmentPreviewDto;
import com.komar.helpdesk.exceptions.NotFoundException;
import com.komar.helpdesk.repository.AttachmentRepository;
import com.komar.helpdesk.repository.model.Attachment;
import com.komar.helpdesk.repository.model.Ticket;
import com.komar.helpdesk.repository.model.User;
import com.komar.helpdesk.service.AttachmentService;
import com.komar.helpdesk.service.EventService;
import com.komar.helpdesk.service.UserService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Маріна
 */

@Service
public class AttachmentServiceImpl implements AttachmentService{
    
    private static  final Logger LOGGER = LogManager.getLogger(AttachmentServiceImpl.class);

    @Autowired
    private AttachmentRepository attachmentRepository;
    
    @Autowired
    private UserService userService;
    
    @Autowired 
    private AttachmentConverter attachmentConverter;
    
    @Autowired
    private EventService eventServise;
    
    @Override
    public AttachmentDto getAttachment(long id, String username) throws AccessDeniedException, NotFoundException {
        LOGGER.info("(SERVICE) Get attachment " + id);
        Attachment attachment = attachmentRepository.get(id);
        if (attachment == null) throw new NotFoundException(Attachment.class, id);
        Ticket ticket = attachment.getTicket();
        User user = userService.getUser(username);
        if (Actions.VIEW.isAllowed(user, ticket)) {
            LOGGER.info("(SERVICE) Return attachment ");
            return attachmentConverter.toDto(attachment);
        }
        LOGGER.info("(SERVICE) Getting Attachment forbidden");
        throw new AccessDeniedException(Actions.VIEW, ticket, user);
    }

    @Override
    public void deleteAttachment(AttachmentPreviewDto attachmentPreviewDto, Ticket ticket, User user) {
        eventServise.fileRemoved(attachmentPreviewDto, user, ticket);
    }

    @Override
    public long addAttachment(AttachmentDto attachmentDto, Ticket ticket, User user) {
        LOGGER.info("(SERVICE) Add attachment " + attachmentDto);
        Attachment attachment = attachmentConverter.toEntity(attachmentDto);
        attachment.setTicket(ticket);
        long id = attachmentRepository.add(attachment);
        eventServise.fileAttached(attachmentDto, user, ticket);
        LOGGER.info("(SERVICE) Attachment added.");
        return id;
    }
    
}
