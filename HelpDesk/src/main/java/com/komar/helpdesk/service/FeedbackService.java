package com.komar.helpdesk.service;

import com.komar.helpdesk.dto.FeedbackDto;
import com.komar.helpdesk.exceptions.AccessDeniedException;
import com.komar.helpdesk.exceptions.NotFoundException;

/**
 *
 * @author Маріна
 */
public interface FeedbackService {
    public FeedbackDto getFeedback(long ticketId, String username) throws AccessDeniedException, NotFoundException ;
    public long addFeedback(FeedbackDto feedbackDto, String username) throws AccessDeniedException, NotFoundException ;
}
