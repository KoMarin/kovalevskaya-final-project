package com.komar.helpdesk.serialize.bytearray;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.File;
import java.io.IOException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


/**
 *
 * @author Маріна
 */
public class ByteArrayDeserializer extends StdDeserializer<byte[]> {

    private static  final Logger LOGGER = LogManager.getLogger(ByteArrayDeserializer.class);
    
    protected ByteArrayDeserializer() {
        super(byte[].class);
    }

    @Override
    public byte[] deserialize(JsonParser parser, DeserializationContext context) throws IOException {
        LOGGER.info("DESERIALIZE");
        String value = parser.readValueAs(String.class);
        value = value.split(",")[1];
        byte[] res = Base64.decodeBase64(value);
        LOGGER.info("DESERIALIZED");
        return res;
    }

    
}
