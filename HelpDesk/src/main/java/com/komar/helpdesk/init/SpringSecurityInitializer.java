
package com.komar.helpdesk.init;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 *
 * @author Маріна
 */
public class SpringSecurityInitializer extends AbstractSecurityWebApplicationInitializer{
    
}
