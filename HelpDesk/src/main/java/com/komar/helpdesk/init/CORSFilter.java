package com.komar.helpdesk.init;

import com.komar.helpdesk.enums.Actions;
import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;

/**
 * Create Filter to handle Same Origin Policy related issues
 */


public class CORSFilter implements Filter {

    private static  final Logger LOGGER = LogManager.getLogger(Actions.class);
    
    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
                    throws IOException, ServletException {
        LOGGER.info("Filter CORS doFilter");
        HttpServletResponse response = (HttpServletResponse) res;
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type,X-Requested-With,Accept,authorization,Origin,Access-Control-Request-Method,Access-Control-Request-Headers");
        chain.doFilter(req, response);
    }

    @Override
    public void init(FilterConfig filterConfig) {
        LOGGER.info("init Filter CORS");
    }

    @Override
    public void destroy() {
        LOGGER.info("destroy Filter CORS");
    }

}