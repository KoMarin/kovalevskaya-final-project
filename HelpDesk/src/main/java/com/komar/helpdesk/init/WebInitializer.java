package com.komar.helpdesk.init;

import com.komar.helpdesk.config.SpringMailConfig;
import com.komar.helpdesk.config.WebConfig;
import com.komar.helpdesk.config.WebSecurityConfig;
import javax.servlet.Filter;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class WebInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[] { SpringSecurityInitializer.class, WebSecurityConfig.class, SpringMailConfig.class };
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[] { WebConfig.class };
    }

    @Override
    protected String[] getServletMappings() {
        return new String[] { "/" };
    }  
    
    @Override
	protected Filter[] getServletFilters() {
            Filter[] singleton = { new CORSFilter() };
            return singleton;
	}
    
}