package com.komar.helpdesk.repository;

import com.komar.helpdesk.repository.model.Attachment;

/**
 *
 * @author Маріна
 */
public interface AttachmentRepository {
    public Attachment get(long id);
    public long add(Attachment attachment);
}
