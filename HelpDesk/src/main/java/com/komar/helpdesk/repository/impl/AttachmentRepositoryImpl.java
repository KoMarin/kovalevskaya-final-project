package com.komar.helpdesk.repository.impl;

import com.komar.helpdesk.repository.AttachmentRepository;
import com.komar.helpdesk.repository.model.Attachment;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Маріна
 */

@Repository
public class AttachmentRepositoryImpl implements AttachmentRepository{

    private static  final Logger LOGGER = LogManager.getLogger(AttachmentRepositoryImpl.class);
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    @Transactional
    public Attachment get(long id) {
        LOGGER.info("(REPO)Get attachment: " + id);
        Session session = sessionFactory.getCurrentSession();
        Attachment attachment = session.get(Attachment.class, id);
        return attachment;
    }

    @Override
    @Transactional
    public long add(Attachment attachment) {
        LOGGER.info("(REPO)SAVE attachment: " + attachment);
        Session session = sessionFactory.getCurrentSession();
        attachment = (Attachment) session.merge(attachment);
        LOGGER.info("Attachment saved.");
        return attachment.getId();
    }

   
    
}
