package com.komar.helpdesk.repository;

import com.komar.helpdesk.enums.Roles;
import com.komar.helpdesk.repository.model.User;
import java.util.List;

/**
 *
 * @author Маріна
 */
public interface UserRepository {
    public User getUser(long id);
    public User getUserByUserName(String username);
    public List<User> getUsersWithRole(Roles role);
}
