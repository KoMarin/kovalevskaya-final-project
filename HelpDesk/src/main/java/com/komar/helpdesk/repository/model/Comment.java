package com.komar.helpdesk.repository.model;

import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="COMMENT")
public class Comment {
    
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Column(name = "DATE")
    private LocalDate date;
    
    @Column(name = "TEXT")
    private String text;
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "TICKET_ID")
    private Ticket ticket;
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "USER_ID")
    private User user;

    public Comment() {
    }
    
    public LocalDate getDate() {
        return date;
    }

    public long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public User getUser() {
        return user;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public void setUser(User user) {
        this.user = user;
    }  

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 67 * hash + Objects.hashCode(this.date);
        hash = 67 * hash + Objects.hashCode(this.text);
        hash = 67 * hash + Objects.hashCode(this.ticket);
        hash = 67 * hash + Objects.hashCode(this.user);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Comment other = (Comment) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        if (!Objects.equals(this.text, other.text)) {
            return false;
        }
        if (!Objects.equals(this.ticket, other.ticket)) {
            return false;
        }
        if (!Objects.equals(this.user, other.user)) {
            return false;
        }
        return true;
    }

    public static class Builder {

        private long id;
        private LocalDate date;
        private String text;
        private Ticket ticket;
        private User user;

        private Builder() {
        }

        public Builder id(final long value) {
            this.id = value;
            return this;
        }

        public Builder date(final LocalDate value) {
            this.date = value;
            return this;
        }

        public Builder text(final String value) {
            this.text = value;
            return this;
        }

        public Builder ticket(final Ticket value) {
            this.ticket = value;
            return this;
        }

        public Builder user(final User value) {
            this.user = value;
            return this;
        }

        public Comment build() {
            return new com.komar.helpdesk.repository.model.Comment(id, date, text, ticket, user);
        }
    }

    public static Comment.Builder builder() {
        return new Comment.Builder();
    }

    private Comment(final long id, final LocalDate date, final String text, final Ticket ticket, final User user) {
        this.id = id;
        this.date = date;
        this.text = text;
        this.ticket = ticket;
        this.user = user;
    }
    
}
