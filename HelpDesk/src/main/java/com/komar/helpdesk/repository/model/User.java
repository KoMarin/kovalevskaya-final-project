package com.komar.helpdesk.repository.model;

import com.komar.helpdesk.enums.Roles;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table(name = "USER")
public class User{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "POSITION")
    private String position;

    @Column(name = "PHONE")
    private String phone;

    @Column(name = "ROLE")
    @Enumerated(EnumType.STRING)
    private Roles role;

    @Column(name = "EMAIL")
    private String email;
    
    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "ADDRESS")
    private String address;
    
    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "OWNER_ID")
    @LazyCollection(LazyCollectionOption.FALSE)
    private Set<Ticket> OwnedTickets; 

    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "APPROVER_ID")
    @LazyCollection(LazyCollectionOption.FALSE)
    private Set<Ticket> ApprovedTickets;
    
    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "ASSIGNEE_ID")
    @LazyCollection(LazyCollectionOption.FALSE)
    private Set<Ticket> assignedTickets;

    public User() {
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Roles getRole() {
        return role;
    }

    public void setRole(Roles role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Set<Ticket> getApprovedTickets() {
        return ApprovedTickets;
    }

    public Set<Ticket> getAssignedTickets() {
        return assignedTickets;
    }

    public Set<Ticket> getOwnedTickets() {
        return OwnedTickets;
    }

    public void setApprovedTickets(Set<Ticket> ApprovedTickets) {
        this.ApprovedTickets = ApprovedTickets;
    }

    public void setAssignedTickets(Set<Ticket> assignedTickets) {
        this.assignedTickets = assignedTickets;
    }

    public void setOwnedTickets(Set<Ticket> OwnedTickets) {
        this.OwnedTickets = OwnedTickets;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public static class Builder {

        private Long id;
        private String firstName;
        private String lastName;
        private String position;
        private String phone;
        private Roles role;
        private String email;
        private String password;
        private String address;
        private Set<Ticket> OwnedTickets;
        private Set<Ticket> ApprovedTickets;
        private Set<Ticket> assignedTickets;

        private Builder() {
        }

        public Builder id(final Long value) {
            this.id = value;
            return this;
        }

        public Builder firstName(final String value) {
            this.firstName = value;
            return this;
        }

        public Builder lastName(final String value) {
            this.lastName = value;
            return this;
        }

        public Builder position(final String value) {
            this.position = value;
            return this;
        }

        public Builder phone(final String value) {
            this.phone = value;
            return this;
        }

        public Builder role(final Roles value) {
            this.role = value;
            return this;
        }

        public Builder email(final String value) {
            this.email = value;
            return this;
        }

        public Builder password(final String value) {
            this.password = value;
            return this;
        }

        public Builder address(final String value) {
            this.address = value;
            return this;
        }

        public Builder OwnedTickets(final Set<Ticket> value) {
            this.OwnedTickets = value;
            return this;
        }

        public Builder ApprovedTickets(final Set<Ticket> value) {
            this.ApprovedTickets = value;
            return this;
        }

        public Builder assignedTickets(final Set<Ticket> value) {
            this.assignedTickets = value;
            return this;
        }

        public User build() {
            return new com.komar.helpdesk.repository.model.User(id, firstName, lastName, position, phone, role, email, password, address, OwnedTickets, ApprovedTickets, assignedTickets);
        }
    }

    public static User.Builder builder() {
        return new User.Builder();
    }

    private User(final Long id, final String firstName, final String lastName, final String position, final String phone, final Roles role, final String email, final String password, final String address, final Set<Ticket> OwnedTickets, final Set<Ticket> ApprovedTickets, final Set<Ticket> assignedTickets) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.position = position;
        this.phone = phone;
        this.role = role;
        this.email = email;
        this.password = password;
        this.address = address;
        this.OwnedTickets = OwnedTickets;
        this.ApprovedTickets = ApprovedTickets;
        this.assignedTickets = assignedTickets;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.id);
        hash = 17 * hash + Objects.hashCode(this.firstName);
        hash = 17 * hash + Objects.hashCode(this.lastName);
        hash = 17 * hash + Objects.hashCode(this.position);
        hash = 17 * hash + Objects.hashCode(this.phone);
        hash = 17 * hash + Objects.hashCode(this.role);
        hash = 17 * hash + Objects.hashCode(this.email);
        hash = 17 * hash + Objects.hashCode(this.password);
        hash = 17 * hash + Objects.hashCode(this.address);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.firstName, other.firstName)) {
            return false;
        }
        if (!Objects.equals(this.lastName, other.lastName)) {
            return false;
        }
        if (!Objects.equals(this.position, other.position)) {
            return false;
        }
        if (!Objects.equals(this.phone, other.phone)) {
            return false;
        }
        if (this.role != other.role) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.password, other.password)) {
            return false;
        }
        if (!Objects.equals(this.address, other.address)) {
            return false;
        }
        return true;
    }



    @Override
    public String toString() {
        return "User{" + "id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", position=" + position + ", phone=" + phone + ", role=" + role + ", email=" + email + ", password=" + password + ", address=" + address + ", OwnedTickets=" + OwnedTickets + ", ApprovedTickets=" + ApprovedTickets + ", assignedTickets=" + assignedTickets + '}';
    }
     
    
    
}
