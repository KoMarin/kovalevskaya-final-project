package com.komar.helpdesk.repository.impl;

import com.komar.helpdesk.repository.TicketRepository;
import com.komar.helpdesk.enums.Roles;
import com.komar.helpdesk.enums.States;
import com.komar.helpdesk.repository.model.Ticket;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Маріна
 */
@Repository
public class TicketRepositoryImpl implements TicketRepository{

    private static  final Logger LOGGER = LogManager.getLogger(TicketRepositoryImpl.class);
    
    @Autowired
    private SessionFactory sessionFactory;
   
    @Override
    @Transactional
    public Set<Ticket> allTickets() {
        LOGGER.info("(REPO) Get all tickets");
        Session session = sessionFactory.getCurrentSession();
        List<Ticket> tickets = session.createQuery("from Ticket").list();
        session.close();
        return new HashSet<>(tickets);
    }

    @Override
    @Transactional
    public Set<Ticket> allEmployeeNewTickets() {
        LOGGER.info("(REPO) Get all Employee New tickets");
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Ticket.class)
                .createAlias("owner", "owner")
                .add(Restrictions.eq("owner.role", Roles.EMPLOYEE))
                .add(Restrictions.eq("status", States.NEW));
        List<Ticket> tickets = criteria.list();
        return new HashSet<>(tickets);
    }

    @Override
    @Transactional
    public Set<Ticket> allApprovedTickets() {
        LOGGER.info("(REPO) Get all Approved tickets");
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Ticket.class)
                .createAlias("owner", "owner")
                .add(Restrictions.eq("status", States.APPROVED));
        List<Ticket> tickets = criteria.list();
        return new HashSet<>(tickets);
    }

    @Override
    @Transactional
    public Ticket getTicket(long id) {
        LOGGER.info("(REPO) Get ticket " + id);
        Session session = sessionFactory.getCurrentSession();
        Ticket ticket = session.get(Ticket.class, id);
        return ticket;
    }

    @Override
    @Transactional
    public Ticket update(Ticket ticket) {
        LOGGER.info("(REPO) Update ticket " + ticket);
        Session session = sessionFactory.getCurrentSession();
        ticket = (Ticket) session.merge(ticket);
        LOGGER.info("(REPO) Ticket was updated.");
        return ticket;
    }

}
