package com.komar.helpdesk.repository;

import com.komar.helpdesk.repository.model.Ticket;
import java.util.Set;

/**
 *
 * @author Маріна
 */
public interface TicketRepository {
    public Set<Ticket> allTickets();
    public Set<Ticket> allEmployeeNewTickets();
    public Set<Ticket> allApprovedTickets();
    public Ticket getTicket(long id);
    public Ticket update(Ticket ticket);
}
