package com.komar.helpdesk.repository.impl;

import com.komar.helpdesk.repository.CategoryRepository;
import com.komar.helpdesk.repository.model.Category;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Маріна
 */

@Repository
public class CategoryRepositoryImpl implements CategoryRepository{
    
    @Autowired
    SessionFactory sessionFactory;
    
    
    @Override
    @Transactional
    public List<Category> allCategories() {
        Session session = sessionFactory.getCurrentSession();
        List<Category> categories = session.createQuery("from Category").list();
        return categories;
    }

    @Override
    @Transactional
    public Category getCategory(long id) {
        Session session = sessionFactory.getCurrentSession();
        Category category = session.get(Category.class, id);
        return category;
    }
    
}
