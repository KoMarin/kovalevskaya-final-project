package com.komar.helpdesk.repository.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="HISTORY")
public class History implements Serializable  {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;
    
    @Column(name = "DATE")
    private LocalDate date;
    
    @Column(name = "ACTION")
    private String action;
    
    @Column(name = "DESCRIPTION")
    private String description;
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "USER_ID")
    private User user; 
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "TICKET_ID")
    private Ticket ticket;

    public History() {
    }

    public String getAction() {
        return action;
    }

    public LocalDate getDate() {
        return date;
    }

    public long getId() {
        return id;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public User getUser() {
        return user;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static class Builder {

        private long id;
        private LocalDate date;
        private String action;
        private String description;
        private User user;
        private Ticket ticket;

        private Builder() {
        }

        public Builder id(final long value) {
            this.id = value;
            return this;
        }

        public Builder date(final LocalDate value) {
            this.date = value;
            return this;
        }

        public Builder action(final String value) {
            this.action = value;
            return this;
        }

        public Builder description(final String value) {
            this.description = value;
            return this;
        }

        public Builder user(final User value) {
            this.user = value;
            return this;
        }

        public Builder ticket(final Ticket value) {
            this.ticket = value;
            return this;
        }

        public History build() {
            return new com.komar.helpdesk.repository.model.History(id, date, action, description, user, ticket);
        }
    }

    public static History.Builder builder() {
        return new History.Builder();
    }

    private History(final long id, final LocalDate date, final String action, final String description, final User user, final Ticket ticket) {
        this.id = id;
        this.date = date;
        this.action = action;
        this.description = description;
        this.user = user;
        this.ticket = ticket;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 97 * hash + Objects.hashCode(this.date);
        hash = 97 * hash + Objects.hashCode(this.action);
        hash = 97 * hash + Objects.hashCode(this.description);
        hash = 97 * hash + Objects.hashCode(this.user);
        hash = 97 * hash + Objects.hashCode(this.ticket);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final History other = (History) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        if (!Objects.equals(this.action, other.action)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.user, other.user)) {
            return false;
        }
        if (!Objects.equals(this.ticket, other.ticket)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "History{" + "id=" + id + ", date=" + date + ", action=" + action + ", description=" + description + ", user=" + user + ", ticket=" + ticket + '}';
    }
 
}
