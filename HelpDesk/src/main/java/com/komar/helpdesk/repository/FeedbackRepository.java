package com.komar.helpdesk.repository;

import com.komar.helpdesk.repository.model.Feedback;

/**
 *
 * @author Маріна
 */
public interface FeedbackRepository {
    public Feedback add(Feedback feedback);
}
