package com.komar.helpdesk.repository;

import com.komar.helpdesk.repository.model.Comment;

/**
 *
 * @author Маріна
 */
public interface CommentRepository {
    public long addComment(Comment comment);
}
