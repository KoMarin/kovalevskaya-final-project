package com.komar.helpdesk.repository.model;

import com.komar.helpdesk.enums.Urgencies;
import com.komar.helpdesk.enums.States;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;   
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 *
 * @author Маріна
 */

@Entity
@Table(name = "TICKET")
public class Ticket {
    
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "NAME")
    private String name;
    
    @Column(name = "DESIRED_RESOLUTION_DATE")
    private LocalDate desiredDate;
    
    @Column(name = "URGENCY")
    @Enumerated(EnumType.STRING)
    private Urgencies urgency;
    
    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    private States status;
   
    @Column(name = "DESCRIPTION")
    private String description;
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "CATEGORY_ID")
    private Category category;

    @Column(name = "CREATED_ON", updatable=false)
    private LocalDate createdOn;
  
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ASSIGNEE_ID")
    private User assignee;
                  
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "OWNER_ID", updatable=false)
    private User owner;
         //fg rrrrrr
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "APPROVER_ID")
    private User approver;
    
    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "TICKET_ID")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Feedback> feedbacks;
    
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "TICKET_ID", updatable = false)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Comment> comments;
    
    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "TICKET_ID", updatable = false)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<History> history;
    
    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "TICKET_ID")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Attachment> attachments;

    public Ticket() {
        attachments = new ArrayList<>();
        history = new ArrayList<>();
    }

    public LocalDate getDesiredDate() {
        return desiredDate;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public States getStatus() {
        return status;
    }

    public Urgencies getUrgency() {
        return urgency;
    }

    public void setDesiredDate(LocalDate date) {
        this.desiredDate = date;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStatus(States status) {
        this.status = status;
    }

    public void setUrgency(Urgencies urgency) {
        this.urgency = urgency;
    }

    public User getApprover() {
        return approver;
    }

    public User getAssignee() {
        return assignee;
    }

    public LocalDate getCreatedOn() {
        return createdOn;  
    }  

    public String getDescription() {
        return description;
    }

    public User getOwner() {
        return owner;
    }

    public void setApprover(User approver) {
        this.approver = approver;
    }

    public void setAssignee(User assignee) {
        this.assignee = assignee;
    }

    public void setCreatedOn(LocalDate createdOn) {
        this.createdOn = createdOn;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<Feedback> getFeedbacks() {
        return feedbacks;
    }

    public void setFeedbacks(List<Feedback> feedbacks) {
        this.feedbacks = feedbacks;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public List<Attachment> getAttachments() {
        return attachments;
    }

    public List<History> getHistory() {
        return history;
    }

    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }

    public void setHistory(List<History> history) {
        this.history = history;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 61 * hash + Objects.hashCode(this.name);
        hash = 61 * hash + Objects.hashCode(this.desiredDate);
        hash = 61 * hash + Objects.hashCode(this.urgency);
        hash = 61 * hash + Objects.hashCode(this.status);
        hash = 61 * hash + Objects.hashCode(this.description);
        hash = 61 * hash + Objects.hashCode(this.category);
        hash = 61 * hash + Objects.hashCode(this.createdOn);
        hash = 61 * hash + Objects.hashCode(this.assignee);
        hash = 61 * hash + Objects.hashCode(this.owner);
        hash = 61 * hash + Objects.hashCode(this.approver);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Ticket other = (Ticket) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.desiredDate, other.desiredDate)) {
            return false;
        }
        if (this.urgency != other.urgency) {
            return false;
        }
        if (this.status != other.status) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.category, other.category)) {
            return false;
        }
        if (!Objects.equals(this.createdOn, other.createdOn)) {
            return false;
        }
        if (!Objects.equals(this.assignee, other.assignee)) {
            return false;
        }
        if (!Objects.equals(this.owner, other.owner)) {
            return false;
        }
        if (!Objects.equals(this.approver, other.approver)) {
            return false;
        }
        return true;
    }

    public static class Builder {

        private long id;
        private String name;
        private LocalDate desiredDate;
        private Urgencies urgency;
        private States status;
        private String description;
        private Category category;
        private LocalDate createdOn;
        private User assignee;
        private User owner;
        private User approver;
        private List<Feedback> feedbacks;
        private List<Comment> comments;
        private List<History> history;
        private List<Attachment> attachments;

        private Builder() {
        }

        public Builder id(final long value) {
            this.id = value;
            return this;
        }

        public Builder name(final String value) {
            this.name = value;
            return this;
        }

        public Builder desiredDate(final LocalDate value) {
            this.desiredDate = value;
            return this;
        }

        public Builder urgency(final Urgencies value) {
            this.urgency = value;
            return this;
        }

        public Builder status(final States value) {
            this.status = value;
            return this;
        }

        public Builder description(final String value) {
            this.description = value;
            return this;
        }

        public Builder category(final Category value) {
            this.category = value;
            return this;
        }

        public Builder createdOn(final LocalDate value) {
            this.createdOn = value;
            return this;
        }

        public Builder assignee(final User value) {
            this.assignee = value;
            return this;
        }

        public Builder owner(final User value) {
            this.owner = value;
            return this;
        }

        public Builder approver(final User value) {
            this.approver = value;
            return this;
        }

        public Builder feedbacks(final List<Feedback> value) {
            this.feedbacks = value;
            return this;
        }

        public Builder comments(final List<Comment> value) {
            this.comments = value;
            return this;
        }

        public Builder history(final List<History> value) {
            this.history = value;
            return this;
        }

        public Builder attachments(final List<Attachment> value) {
            this.attachments = value;
            return this;
        }

        public Ticket build() {
            return new com.komar.helpdesk.repository.model.Ticket(id, name, desiredDate, urgency, status, description, category, createdOn, assignee, owner, approver, feedbacks, comments, history, attachments);
        }
    }

    public static Ticket.Builder builder() {
        return new Ticket.Builder();
    }

    private Ticket(final long id, final String name, final LocalDate desiredDate, final Urgencies urgency, final States status, final String description, final Category category, final LocalDate createdOn, final User assignee, final User owner, final User approver, final List<Feedback> feedbacks, final List<Comment> comments, final List<History> history, final List<Attachment> attachments) {
        this.id = id;
        this.name = name;
        this.desiredDate = desiredDate;
        this.urgency = urgency;
        this.status = status;
        this.description = description;
        this.category = category;
        this.createdOn = createdOn;
        this.assignee = assignee;
        this.owner = owner;
        this.approver = approver;
        this.feedbacks = feedbacks;
        this.comments = comments;
        this.history = history;
        this.attachments = attachments;  
    }

    @Override
    public String toString() {
        return "Ticket{" + "id=" + id + ", name=" + name 
                + ", desiredDate=" + desiredDate + ", urgency=" + urgency + ", status=" + status 
                + ", description=" + description + ", category=" + category + ", createdOn=" + createdOn 
                + ", assignee=" + ( (assignee != null) ? assignee.getFirstName() : "null") 
                + ", owner=" + ( (owner != null) ? owner.getFirstName() : "null")    
                + ", approver=" + ( (approver != null) ? approver.getFirstName() : "null") + '}';
    }
    
}
