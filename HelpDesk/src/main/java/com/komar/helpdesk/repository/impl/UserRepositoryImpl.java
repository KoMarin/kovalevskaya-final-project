package com.komar.helpdesk.repository.impl;

import com.komar.helpdesk.repository.UserRepository;
import com.komar.helpdesk.enums.Roles;
import com.komar.helpdesk.repository.model.User;
import java.util.List;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class UserRepositoryImpl implements UserRepository{
    
    private static  final Logger LOGGER = LogManager.getLogger(UserRepositoryImpl.class);
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    @Transactional
    public User getUser(long id) {
        LOGGER.info("(REPO) Get user " + id);
        Session session = sessionFactory.getCurrentSession();
        User user = session.get(User.class, id);
        return user;
    }
    
    @Override
    @Transactional
    public User getUserByUserName(String username) {
        LOGGER.info("(REPO) Get user " + username);
        Session session = sessionFactory.getCurrentSession();
        List<User> users = session
                .createQuery("from User where email=?")
                .setParameter(0, username)
                .list();
        User user = 
                (users.size() > 0) ? 
                users.get(0) 
                : null;
        return user;
    }

    @Override
    @Transactional
    public List<User> getUsersWithRole(Roles role) {
        LOGGER.info("(REPO) Get users with role " + role);
        Session session = sessionFactory.getCurrentSession();
        List<User> users = session
                .createQuery("from User where role=?")
                .setParameter(0, role)
                .list();
        return users;
    }
}
