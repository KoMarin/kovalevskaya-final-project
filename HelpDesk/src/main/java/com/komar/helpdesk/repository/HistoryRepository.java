package com.komar.helpdesk.repository;

import com.komar.helpdesk.repository.model.History;
import java.util.List;

/**
 *
 * @author Маріна
 */
public interface HistoryRepository {
   public long add(History history);
}
