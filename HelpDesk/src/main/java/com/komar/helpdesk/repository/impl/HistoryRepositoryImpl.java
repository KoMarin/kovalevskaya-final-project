package com.komar.helpdesk.repository.impl;

import com.komar.helpdesk.repository.HistoryRepository;
import com.komar.helpdesk.repository.model.History;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Маріна
 */

@Repository
public class HistoryRepositoryImpl implements HistoryRepository{
    
    private static  final Logger LOGGER = LogManager.getLogger(HistoryRepositoryImpl.class);
    
    @Autowired
    private SessionFactory sessionFactory;
  
    @Override
    @Transactional
    public long add(History history) {
        LOGGER.info("(REPO) Add history" + history);
        
        Session session = sessionFactory.getCurrentSession();

        history = (History)session.merge(history);
        
        LOGGER.info("History added.");
        return history.getId();
    }
    
}
