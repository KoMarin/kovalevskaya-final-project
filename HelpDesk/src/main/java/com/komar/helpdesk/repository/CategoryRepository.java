package com.komar.helpdesk.repository;

import com.komar.helpdesk.repository.model.Category;
import java.util.List;

/**
 *
 * @author Маріна
 */
public interface CategoryRepository {
    public List<Category> allCategories();
    public Category getCategory(long id);
}
