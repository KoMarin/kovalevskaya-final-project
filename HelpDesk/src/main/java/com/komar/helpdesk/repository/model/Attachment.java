package com.komar.helpdesk.repository.model;

import java.util.Arrays;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ATTACHMENT")
public class Attachment {  
    
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Column(name = "NAME")
    private String name;
    
    @Column(name = "BLOB")
    @Lob
    private byte[] blob;
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "TICKET_ID")
    private Ticket ticket;

    public byte[] getBlob() {
        return blob;
    }

    public long getId() {
        return id;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setBlob(byte[] blob) {
        this.blob = blob;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public Attachment() {
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 41 * hash + Arrays.hashCode(this.blob);
        hash = 41 * hash + Objects.hashCode(this.ticket);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Attachment other = (Attachment) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Arrays.equals(this.blob, other.blob)) {
            return false;
        }
        if (!Objects.equals(this.ticket, other.ticket)) {
            return false;
        }
        return true;
    }

    public static class Builder {

        private long id;
        private String name;
        private byte[] blob;
        private Ticket ticket;

        private Builder() {
        }

        public Builder id(final long value) {
            this.id = value;
            return this;
        }

        public Builder name(final String value) {
            this.name = value;
            return this;
        }

        public Builder blob(final byte[] value) {
            this.blob = value;
            return this;
        }

        public Builder ticket(final Ticket value) {
            this.ticket = value;
            return this;
        }

        public Attachment build() {
            return new com.komar.helpdesk.repository.model.Attachment(id, name, blob, ticket);
        }
    }

    public static Attachment.Builder builder() {
        return new Attachment.Builder();
    }

    private Attachment(final long id, final String name, final byte[] blob, final Ticket ticket) {
        this.id = id;
        this.name = name;
        this.blob = blob;
        this.ticket = ticket;
    }
  
    @Override
    public String toString() {
        return "Attachment{" + "id=" + id + ", name=" + name + ", blob=" + blob + ", ticket=" + (ticket!=null) + '}';
    }
    
    
    
}
