package com.komar.helpdesk.repository.model;

import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Column;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Table(name="FEEDBACK")
public class Feedback {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;
    
    @Column(name = "RATE")
    private int rate;
    
    @Column(name = "DATE")
    private LocalDate date;
    
    @Column(name = "TEXT")
    private String text;
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "USER_ID")
    private User user;
    
    
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "TICKET_ID")
    private Ticket ticket;

    public Feedback() {
    }
  
    public LocalDate getDate() {
        return date;
    }

    public long getId() {
        return id;
    }

    public int getRate() {
        return rate;
    }

    public String getText() {
        return text;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public User getUser() {
        return user;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 17 * hash + this.rate;
        hash = 17 * hash + Objects.hashCode(this.date);
        hash = 17 * hash + Objects.hashCode(this.text);
        hash = 17 * hash + Objects.hashCode(this.user);
        hash = 17 * hash + Objects.hashCode(this.ticket);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Feedback other = (Feedback) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.rate != other.rate) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        if (!Objects.equals(this.text, other.text)) {
            return false;
        }
        if (!Objects.equals(this.user, other.user)) {
            return false;
        }
        if (!Objects.equals(this.ticket, other.ticket)) {
            return false;
        }
        return true;
    }

    public static class Builder {

        private long id;
        private int rate;
        private LocalDate date;
        private String text;
        private User user;
        private Ticket ticket;

        private Builder() {
        }

        public Builder id(final long value) {
            this.id = value;
            return this;
        }

        public Builder rate(final int value) {
            this.rate = value;
            return this;
        }

        public Builder date(final LocalDate value) {
            this.date = value;
            return this;
        }

        public Builder text(final String value) {
            this.text = value;
            return this;
        }

        public Builder user(final User value) {
            this.user = value;
            return this;
        }

        public Builder ticket(final Ticket value) {
            this.ticket = value;
            return this;
        }

        public Feedback build() {
            return new com.komar.helpdesk.repository.model.Feedback(id, rate, date, text, user, ticket);
        }
    }

    public static Feedback.Builder builder() {
        return new Feedback.Builder();
    }

    private Feedback(final long id, final int rate, final LocalDate date, final String text, final User user, final Ticket ticket) {
        this.id = id;
        this.rate = rate;
        this.date = date;
        this.text = text;
        this.user = user;
        this.ticket = ticket;
    }
    
}
