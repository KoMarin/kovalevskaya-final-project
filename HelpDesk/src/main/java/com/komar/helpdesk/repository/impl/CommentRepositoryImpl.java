package com.komar.helpdesk.repository.impl;

import com.komar.helpdesk.repository.CommentRepository;
import com.komar.helpdesk.repository.TicketRepository;
import com.komar.helpdesk.repository.model.Comment;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Session;       
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Маріна
 */

@Repository
public class CommentRepositoryImpl implements CommentRepository{
    
    private static  final Logger LOGGER = LogManager.getLogger(CommentRepositoryImpl.class);
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    @Transactional
    public long addComment(Comment comment) {
        LOGGER.info("(REPO) Add comment: " + comment);
        Session session = sessionFactory.getCurrentSession();
        session.merge(comment);
        LOGGER.info("comment added");
        return comment.getId();
    }
    
}
