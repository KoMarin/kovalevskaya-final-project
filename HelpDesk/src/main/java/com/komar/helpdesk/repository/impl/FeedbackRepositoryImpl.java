package com.komar.helpdesk.repository.impl;

import com.komar.helpdesk.enums.Actions;
import com.komar.helpdesk.exceptions.CannotExecuteActionExeption;
import com.komar.helpdesk.repository.FeedbackRepository;
import com.komar.helpdesk.repository.TicketRepository;
import com.komar.helpdesk.repository.model.Feedback;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Маріна
 */

@Repository
public class FeedbackRepositoryImpl implements FeedbackRepository{
    
    private static  final Logger LOGGER = LogManager.getLogger(FeedbackRepositoryImpl.class);
    
    @Autowired
    SessionFactory sessionFactory;              
    
    @Autowired
    TicketRepository ticketRepository;

    @Override
    @Transactional
    public Feedback add(Feedback feedback) {
        LOGGER.info("(REPO) Add feedback" + feedback);
        Session session = sessionFactory.getCurrentSession();
        feedback = (Feedback) session.merge(feedback);
        feedback.getTicket().getFeedbacks().add(feedback);
        try {
            Actions.ADD_FEEDBACK.doIt(feedback.getTicket(), feedback.getUser());
            LOGGER.info("Ticket Status was updated");
        } catch (CannotExecuteActionExeption ex) {
            LOGGER.error("Ticket status wasn't updated");
        }
        LOGGER.info("Feedback added.");
        return feedback;
    }

    
}
