package com.komar.helpdesk.exceptions;

import com.komar.helpdesk.enums.Actions;
import com.komar.helpdesk.repository.model.Ticket;
import com.komar.helpdesk.repository.model.User;

/**
 *
 * @author Маріна
 */
public class AccessDeniedException extends Exception{
    
    private Actions action;
    private Ticket ticket;
    private User user;
    
    public AccessDeniedException(){
        super("Current action with current ticket isn't allowed to current user.");
    }
    
    public AccessDeniedException(Actions action){
        super("Action \'" + action.getAction() + "\' with current ticket  isn't allowed to current user.");
        this.action = action;
    }
    
    public AccessDeniedException(Actions action, Ticket ticket){
        super("Action \'" + action.getAction() + "\' with ticket " + ticket + " isn't allowed to current user.");
        this.action = action;
        this.ticket = ticket;
    }
    
    public AccessDeniedException(Actions action, Ticket ticket, User user){
        super("Action \'" + action.getAction() + "\' with ticket " + ticket + " isn't allowed to user " + user);
        this.action = action;
        this.ticket = ticket;
        this.user = user;
    }

    public Actions getAction() {
        return action;
    }

    public User getUser() {
        return user;
    }

    public Ticket getTicket() {
        return ticket;
    }
    
}
