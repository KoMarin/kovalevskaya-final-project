package com.komar.helpdesk.exceptions;

/**
 *
 * @author Маріна
 */
public class NotFoundException extends Exception{
    Class targetClass;
    long ticketId;
    long itemId;

    public NotFoundException(long ticketId) {
        super("Ticket " + ticketId + "not found");
        this.ticketId = ticketId;
    }

    public NotFoundException(Class targetClass, long ticketId) {
        super(targetClass + "not found for ticket " + ticketId);
        this.targetClass = targetClass;
        this.ticketId = ticketId;
    }

    public NotFoundException(Class targetClass, long ticketId, long itemId) {
        super(targetClass + " " + itemId + "not found for ticket " + ticketId);
        this.targetClass = targetClass;
        this.ticketId = ticketId;
        this.itemId = itemId;
    }

    public long getItemId() {
        return itemId;
    }

    public long getTicketId() {
        return ticketId;
    }

    public Class getTargetClass() {
        return targetClass;
    }
 
}
