package com.komar.helpdesk.exceptions;

import com.komar.helpdesk.enums.Actions;
import com.komar.helpdesk.repository.model.Ticket;

/**
 *
 * @author Маріна
 */
public class CannotExecuteActionExeption extends Exception{
    private Actions action;
    private Ticket ticket;

    public CannotExecuteActionExeption(Actions action) {
        super("Action \'" + action.getAction() + "\' cannot be executed" + 
                ((action.getResultState() == null)?
                ": action have not result state and  means only user's perfomance.":
                "."));
        this.action = action;
    }

    public CannotExecuteActionExeption(Actions action, Ticket ticket, String reason) {
        super("Action \'" + action.getAction() + "\' cannot be executed: " + reason);
        this.action = action;
        this.ticket = ticket;
    }

    public Actions getAction() {
        return action;
    }

    public Ticket getTicket() {
        return ticket;
    }
 
}
