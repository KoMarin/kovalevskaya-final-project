package com.komar.helpdesk.controllers;

import javax.servlet.http.HttpServletRequest;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Маріна
 */

@Controller
public class LoginController {
    
    @RequestMapping(path = "/login", method = RequestMethod.GET)
    public String login(
        @RequestParam(value = "error", required = false) String error,
        @RequestParam(value = "logout", required = false) String logout, Model model) {

        if (error != null) {
                model.addAttribute("error", true);
        }

        if (logout != null) {
                model.addAttribute("msg", true);
        }

        return "login";

    }
    
    @RequestMapping("/403")
    public String forbidden(Authentication authentication, HttpServletRequest request){
        return("403");
    }
    
    @RequestMapping("/404")
    public String notFound(Authentication authentication, HttpServletRequest request){
        return("404");
    }
}
