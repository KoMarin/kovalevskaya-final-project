package com.komar.helpdesk.controllers;

import com.komar.helpdesk.dto.CommentDto;
import com.komar.helpdesk.exceptions.AccessDeniedException;
import com.komar.helpdesk.exceptions.NotFoundException;
import com.komar.helpdesk.service.CommentService;
import java.util.List;
import javax.validation.Valid;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Маріна
 */
 
@RestController
@RequestMapping(path = "/api/ticket/{id}/comment")
public class CommentRestController {
    
    private static  final Logger LOGGER = LogManager.getLogger(CommentRestController.class);
   
    @Autowired
    private CommentService commentService;
    
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<CommentDto>> comments(
            @PathVariable(name = "id") long id,
            Authentication authentication){
        try {
            List<CommentDto> comments = commentService.getComments(id, authentication.getName());
            return new ResponseEntity<>(comments, HttpStatus.OK);
        } catch (AccessDeniedException ex) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } catch (NotFoundException ex) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity add(  
            @PathVariable(name = "id") long id, 
            @Valid @RequestBody CommentDto comment,
            Errors errors,
            Authentication authentication){

        if (errors.hasErrors()) {
            LOGGER.info("Validation failed: " + errors);
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        
        try {
            commentService.addComment(authentication.getName(), comment.getText(), id);
            return new ResponseEntity(HttpStatus.OK);
        } catch (AccessDeniedException ex) {
            LOGGER.info("Acces denied: " + ex);
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        } catch (NotFoundException ex) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        
    }
}
