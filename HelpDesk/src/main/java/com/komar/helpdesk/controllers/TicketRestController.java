package com.komar.helpdesk.controllers;

import com.komar.helpdesk.exceptions.AccessDeniedException;
import com.komar.helpdesk.dto.TicketEditionDto;
import com.komar.helpdesk.dto.TicketForEditDto;
import com.komar.helpdesk.dto.TicketOverviewDto;
import com.komar.helpdesk.dto.TicketPreviewDto;
import com.komar.helpdesk.exceptions.CannotExecuteActionExeption;
import com.komar.helpdesk.repository.model.Category;
import com.komar.helpdesk.enums.States;
import com.komar.helpdesk.exceptions.NotFoundException;
import com.komar.helpdesk.service.CategoryService;
import com.komar.helpdesk.service.TicketService;
import java.util.List;
import javax.validation.Valid;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Маріна
 */

@RestController
@RequestMapping(path = "api/ticket")
public class TicketRestController {
    
    private static  final Logger LOGGER = LogManager.getLogger(TicketRestController.class);
          
    @Autowired
    private TicketService ticketService;
    
    @Autowired
    private CategoryService categoryService;
    
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<TicketPreviewDto>> tickets(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        return new ResponseEntity(ticketService.getAllTickets(username), HttpStatus.OK);
    }    
    
    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<TicketOverviewDto> get(@PathVariable(name = "id") long id){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        try {
            TicketOverviewDto ticketOverviewDto = ticketService.getTicketOverview(id, authentication.getName());
            return new ResponseEntity(ticketOverviewDto, HttpStatus.OK);
        } catch (AccessDeniedException ex) {
            LOGGER.info("Acces denied: " + ex);
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } catch (NotFoundException ex) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    
    @RequestMapping(path = "/{id}/edit", method = RequestMethod.GET)
    public ResponseEntity<TicketEditionDto> edit(@PathVariable(name = "id") long id){   
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        try {
            TicketForEditDto ticketDto = ticketService.getTicketForEdit(id, authentication.getName());
            
            List<Category> categories = categoryService.getAllCategories();
            TicketEditionDto ticketEdition = new TicketEditionDto(ticketDto, categories);
            
            return new ResponseEntity(ticketEdition, HttpStatus.OK);
        } catch (AccessDeniedException ex) {
            LOGGER.info("Acces denied: " + ex);
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } catch (NotFoundException ex) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ResponseEntity<TicketEditionDto> create(){
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();
        List<Category> categories = categoryService.getAllCategories();
        
        TicketForEditDto ticketDto = new TicketForEditDto(authentication.getName());
        TicketEditionDto ticketEdition = new TicketEditionDto(ticketDto, categories);
        
        return new ResponseEntity(ticketEdition, HttpStatus.OK);
    }
    
    @RequestMapping(method = RequestMethod.PUT)  
    public ResponseEntity<Long> edit(
            @Valid @RequestBody TicketForEditDto ticket, 
            Errors errors) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        
        String expectedState = ticket.getStatus();
        
        if (errors.hasErrors()) {
            
            LOGGER.info("validation failed:" + errors);
            
            if (ticket.getStatus().equals(States.NEW.getName())) {
                LOGGER.info("return BAD_REQUEST status");
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
            
            LOGGER.info("DRAFT_INVALID state set");
            ticket.setStatus(States.DRAFT_INVALID.name());
        }
        else {
            ticket.setStatus(States.DRAFT.getName());
        }

        LOGGER.info("EDIT" + ticket);

        try {
            ticketService.edit(ticket, expectedState, authentication.getName());
            return new ResponseEntity(ticket.getId(), HttpStatus.OK); 
        } catch (AccessDeniedException ex) {
            LOGGER.info("Acces denied: " + ex);
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
 
    }
    
    @RequestMapping(method = RequestMethod.POST)//, headers = "Accept: */*")  
    public ResponseEntity<Long> create(@Valid @RequestBody TicketForEditDto ticket, 
            Errors errors) {
        
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String expectedState = ticket.getStatus();

        if (errors.hasErrors()) {
            
            LOGGER.info("validation failed:" + errors);
            
            if (ticket.getStatus().equals(States.NEW.getName())) {
                LOGGER.info("return BAD_REQUEST status");
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
            
            LOGGER.info("DRAFT_INVALID state set");
            ticket.setStatus(States.DRAFT_INVALID.name());
        } else {
            ticket.setStatus(States.DRAFT.name());
        }
        
        LOGGER.info("ADD: " + ticket);
        try{
            long id = ticketService.add(ticket, expectedState, authentication.getName());
            ticket.setId(id);
            return new ResponseEntity(id, HttpStatus.OK);
        }catch (AccessDeniedException ex){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }
    
    @RequestMapping(value = "/{id}/{action}", method = RequestMethod.POST)
    public ResponseEntity statusTransaction(@PathVariable(name = "id") long id, 
            @PathVariable(name = "action") String state) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        try {
            ticketService.transactTicketStatus(id, state, authentication.getName());
            return new ResponseEntity(HttpStatus.OK);
        } catch (AccessDeniedException ex) {
            LOGGER.info("Acces denied: " + ex);
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        } catch (CannotExecuteActionExeption ex) {
            LOGGER.warn("Wrong usage of action " + ex);
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        } catch (NotFoundException ex) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }  
    }

}
