package com.komar.helpdesk.controllers;

import com.komar.helpdesk.dto.FeedbackDto;
import com.komar.helpdesk.exceptions.AccessDeniedException;
import com.komar.helpdesk.exceptions.NotFoundException;
import com.komar.helpdesk.service.EventService;
import com.komar.helpdesk.service.FeedbackService;
import javax.validation.Valid;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Маріна
 */

@RestController
@RequestMapping("api/ticket/{id}/feedback") 
public class FeedbackRestController {
    
    private static  final Logger LOGGER = LogManager.getLogger(FeedbackRestController.class);
    
    @Autowired
    private FeedbackService feedbackService;

    @Autowired
    private EventService eventService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<FeedbackDto> getFeedback(
            @PathVariable(name = "id") long id,
            Authentication authentication){
        try {
            return new ResponseEntity<>(feedbackService.getFeedback(id, authentication.getName()), HttpStatus.OK);
        } catch (AccessDeniedException ex) {
            LOGGER.info("Acces denied: " + ex);
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } catch (NotFoundException ex) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity add(
            @Valid @RequestBody FeedbackDto feedback, 
            Errors errors,
            Authentication  authentication){
       
        if (errors.hasErrors()) {
            LOGGER.info("feedback validation errors: " + errors);
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        
        try {
            feedbackService.addFeedback(feedback, authentication.getName());
            return new ResponseEntity(HttpStatus.OK);
        } catch (AccessDeniedException ex) {
            LOGGER.info("Acces denied: " + ex);
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        } catch (NotFoundException ex) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}          
