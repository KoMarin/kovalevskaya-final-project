package com.komar.helpdesk.controllers;

import com.komar.helpdesk.exceptions.AccessDeniedException;
import com.komar.helpdesk.dto.AttachmentDto;
import com.komar.helpdesk.exceptions.NotFoundException;
import com.komar.helpdesk.service.AttachmentService;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**  
 * 
 * @author Маріна
 */

@RestController
@RequestMapping(path = "api/ticket/{ticketId}/attachment/{id}")
public class AttachmentRestController {
    
    private static  final Logger LOGGER = LogManager.getLogger(AttachmentRestController.class);
    
    @Autowired  
    private AttachmentService attachmentService;
    
    @RequestMapping(method = RequestMethod.GET)
    public void edit(@PathVariable(name = "id") long id, 
            Authentication authentication, 
            HttpServletResponse response) throws IOException {
        
        try {            
            AttachmentDto attachmentDto = attachmentService.getAttachment(id, authentication.getName());
            
            InputStream outputStream = new ByteArrayInputStream(attachmentDto.getBlob());
            IOUtils.copy(outputStream, response.getOutputStream());
            
        } catch (AccessDeniedException ex) {
            LOGGER.info("Acces denied: " + ex);
            response.setStatus(HttpStatus.FORBIDDEN.value());
        } catch (NotFoundException ex) {
            LOGGER.info("File not found: " + ex);
            response.setStatus(HttpStatus.NOT_FOUND.value());
        }

    }
}
