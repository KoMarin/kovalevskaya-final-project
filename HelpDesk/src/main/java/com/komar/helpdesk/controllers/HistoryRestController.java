package com.komar.helpdesk.controllers;

import com.komar.helpdesk.dto.HistoryDto;
import com.komar.helpdesk.exceptions.AccessDeniedException;
import com.komar.helpdesk.exceptions.NotFoundException;
import com.komar.helpdesk.service.HistoryService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Маріна
 */

@RestController
@RequestMapping("/api/ticket/{id}/history")
public class HistoryRestController {
    
    @Autowired
    private HistoryService historyService;
    
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<HistoryDto>> getHistory(
            @PathVariable(name = "id") long id,
            Authentication authentication){
        try {
            List<HistoryDto> history = historyService.getHistory(id, authentication.getName());
            return new ResponseEntity<>(history, HttpStatus.OK);
        } catch (AccessDeniedException ex) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } catch (NotFoundException ex) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    
}
