package com.komar.helpdesk.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Маріна
 */

@Controller
@RequestMapping//(path = "/ticket")
public class TicketController {
    
    @RequestMapping(path = {"/", "/ticket"})
    public String list(){
        return "list";
    }
    
    @RequestMapping(path = "/ticket/create")
    public String create(){
        return "edit";
    }
    
    @RequestMapping(path = "/ticket/{id}")
    public String overview(){
        return "overview";
    }
    
    @RequestMapping(path = "/ticket/{id}/edit")
    public String edit(@PathVariable(name = "id") long id){
        return "edit";
    }
    
    @RequestMapping(path = "/ticket/{id}/feedback")
    public String feedback(@PathVariable(name = "id") long id){
        return "feedback";
    }

}
