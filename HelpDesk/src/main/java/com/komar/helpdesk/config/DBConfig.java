package com.komar.helpdesk.config;

import java.util.Properties;
import javax.sql.DataSource;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 * @author Маріна
 */

@Configuration
@EnableTransactionManagement
@PropertySource({ "classpath:database/db.properties" })
@ComponentScan({ "com.komar.helpdesk.repository" })
public class DBConfig {
    
    private static  final Logger LOGGER = LogManager.getLogger(DBConfig.class);
    
    @Autowired
    private Environment env;

    @Bean
    public LocalSessionFactoryBean sessionFactory() {
       LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
       sessionFactory.setDataSource(restDataSource());
       sessionFactory.setPackagesToScan(
         new String[] { "com.komar.helpdesk.repository.model" });
       sessionFactory.setHibernateProperties(hibernateProperties());
       
       LOGGER.info("SESSION FACTORY CREATED " + sessionFactory);
       LOGGER.info("SESSION FACTORY CREATED " + sessionFactory);
       
       return sessionFactory;
    }
  
    @Bean
    public DataSource restDataSource() {
        System.out.println("Hm Hm");
        
        BasicDataSource dataSource = new BasicDataSource();
       dataSource.setDriverClassName(env.getProperty("jdbc.driverClassName"));
       dataSource.setUrl(env.getProperty("jdbc.url"));
       dataSource.setUsername(env.getProperty("jdbc.user"));
       dataSource.setPassword(env.getProperty("jdbc.pass"));

        System.out.println(env.getProperty("jdbc.driverClassName"));
        System.out.println(env.getProperty("jdbc.url"));
        System.out.println(env.getProperty("jdbc.user"));
        System.out.println(env.getProperty("jdbc.pass"));

       return dataSource;
    }

    @Bean
    @Autowired
    public HibernateTransactionManager transactionManager(
      SessionFactory sessionFactory) {

       HibernateTransactionManager txManager
        = new HibernateTransactionManager();
       txManager.setSessionFactory(sessionFactory);

       return txManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
       return new PersistenceExceptionTranslationPostProcessor();
    }

    Properties hibernateProperties() {
        Properties properties =  new Properties() {
          {
             setProperty("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
             setProperty("hibernate.dialect", env.getProperty("hibernate.dialect"));
             setProperty("hibernate.event.merge.entity_copy_observer", env.getProperty("hibernate.event.merge.entity_copy_observer"));
             setProperty("hibernate.globally_quoted_identifiers", "true");
          }
       };
        System.out.println("P: "  + properties);
        return properties;
    }
    
}
