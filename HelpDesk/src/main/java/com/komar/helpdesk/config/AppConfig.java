package com.komar.helpdesk.config;

import com.komar.helpdesk.service.impl.UserDetailsServiceImpl;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;   
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**  
 *
 * @author Маріна
 */

@Configuration
@EnableTransactionManagement
@ComponentScan({"com.komar.helpdesk"})
public class AppConfig {
    
    private static  final Logger LOGGER = LogManager.getLogger(AppConfig.class);
      
    @Bean
    @Qualifier("userDetailsServiceImpl")
    public UserDetailsService userDetailsService(){   
        LOGGER.info("Getting user details service");
        return new UserDetailsServiceImpl();
    }
    
//    @Bean
//    public SessionFactory sessionFactory(){
//        DriverManagerDataSource dataSource = new DriverManagerDataSource();
//       // dataSource.set
//        LOGGER.info("GET Hibernate SESSION factory");
//        try{
//            return  new org.hibernate.cfg.Configuration().configure("/hibernate.cfg.xml").buildSessionFactory();
//        } catch (Throwable ex) {         
//            LOGGER.error("ERROR while getting session factory: " + ex);
//            throw new ExceptionInInitializerError(ex);
//        }
//    }
    
}
