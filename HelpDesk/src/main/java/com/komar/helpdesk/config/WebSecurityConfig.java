package com.komar.helpdesk.config;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


/**
 *
 * @author Маріна
 */
@Configuration
@EnableWebSecurity
@ComponentScan({"com.komar.helpdesk"})
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{
    
    private static  final Logger LOGGER = LogManager.getLogger(WebSecurityConfig.class);
    
    @Autowired
    @Qualifier("userDetailsServiceImpl")
    private UserDetailsService userDetailsService;
    
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(this.passwordEncoder());
        LOGGER.info("SPRING SEQURITY configureGlobal");
    }
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
            .antMatchers("/").hasAuthority("Read")
            .antMatchers("/ticket/").hasAuthority("Read")
            .antMatchers("/ticket/create").hasAuthority("Create")
            .antMatchers("/ticket/**").hasAuthority("Read") 
            .antMatchers("/ticket/**/edit").hasAuthority("Create")
            .antMatchers("/ticket/**/comment/**").hasAuthority("Read")
            .antMatchers("/ticket/**/history").hasAuthority("Read")
            .antMatchers("/ticket/**/feedback").hasAuthority("Read")
            .antMatchers("/api/ticket/").hasAuthority("Read")
            .antMatchers("/api/ticket/**").hasAuthority("Read") 
            .antMatchers("/api/ticket/create").hasAuthority("Create") 
            .antMatchers("/api/ticket/**/edit").hasAuthority("Create")
            .antMatchers("/api/ticket/**/feedback/").hasAuthority("Read")
            .antMatchers("/api/ticket/**/attachment/**").hasAuthority("Read")
            .antMatchers("/api/ticket/**/comment/").hasAuthority("Read")
            .antMatchers("/api/ticket/**/history").hasAuthority("Read")
            .and().formLogin()
            .loginPage("/login")
            .loginProcessingUrl("/login").failureUrl("/login?error")
            .usernameParameter("username")
            .passwordParameter("password")
            .defaultSuccessUrl("/ticket/")
            .and().logout().logoutSuccessUrl("/login?logout")
            .and().csrf().disable()
            .exceptionHandling().accessDeniedPage("/403");
        LOGGER.info("SPRING SEQURITY configure");
    }
    
    @Bean
    public PasswordEncoder passwordEncoder(){
            PasswordEncoder encoder = new BCryptPasswordEncoder();
            System.out.println("Password" + encoder.encode("P@ssword1"));
            return encoder;
    }
}
