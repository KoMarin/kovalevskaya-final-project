package com.komar.helpdesk.convert;

import com.komar.helpdesk.dto.FeedbackDto;
import com.komar.helpdesk.repository.model.Feedback;
import java.time.LocalDate;
import org.springframework.stereotype.Component;

/**
 *
 * @author Маріна
 */

@Component
public class FeedbackConverter {
    public Feedback toEntity(FeedbackDto feedback){
        return Feedback.builder()
                .date(LocalDate.now())
                .rate(feedback.getRate())
                .text(feedback.getText())
                .build();
    }
    
    public FeedbackDto toDto(Feedback feedback){
        return FeedbackDto.builder()
                .rate(feedback.getRate())
                .text(feedback.getText())
                .ticketId(feedback.getTicket().getId())
                .ticketName(feedback.getTicket().getName())
                .build();
    }
}
