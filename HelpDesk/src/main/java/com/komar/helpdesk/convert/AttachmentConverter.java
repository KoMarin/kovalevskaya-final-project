package com.komar.helpdesk.convert;

import com.komar.helpdesk.dto.AttachmentDto;
import com.komar.helpdesk.dto.AttachmentPreviewDto;
import com.komar.helpdesk.repository.AttachmentRepository;
import com.komar.helpdesk.repository.model.Attachment;
import com.komar.helpdesk.repository.model.Ticket;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Маріна
 */

@Component
public class AttachmentConverter {
    
    @Autowired 
    private AttachmentRepository attachmentRepository;
    
    public AttachmentPreviewDto toPreviewDto(Attachment attachment){
        return AttachmentPreviewDto.builder()
                .name(attachment.getName())
                .id(attachment.getId())
                .build();
    }
    
    public Attachment toEntity(AttachmentPreviewDto attachment){
        return attachmentRepository.get(attachment.getId());
    }
    
    public AttachmentDto toDto(Attachment attachment){
        return AttachmentDto.builder()
                .name(attachment.getName())
                .blob(attachment.getBlob())             
                .build();
    }
    
    public Attachment toEntity(AttachmentDto attachmentDto){
        return Attachment.builder()
                .blob(attachmentDto.getBlob())
                .name(attachmentDto.getName())
                .build();
    }
    
    public Attachment toEntity(AttachmentDto attachmentDto, Ticket ticket){
        return Attachment.builder()
                .blob(attachmentDto.getBlob())
                .name(attachmentDto.getName())
                .ticket(ticket)
                .build();
    }
    
    public List<AttachmentPreviewDto> toPreviewDtoList(List<Attachment> attachments){
        return attachments.stream()
                .map(attachment -> toPreviewDto(attachment))
                .collect(Collectors.toList());
    }
        
    public List<Attachment> toEntityList(List<AttachmentPreviewDto> attachments){
        return attachments.stream()
                .map(attachment -> AttachmentConverter.this.toEntity(attachment))
                .collect(Collectors.toList());
    }
    
    public List<Attachment> toEntitytList(List<AttachmentDto> attachments, Ticket ticket){
        return attachments.stream()
                .filter(attachment -> attachment.isLoaded())
                .map(attachment -> toEntity(attachment, ticket))
                .collect(Collectors.toList());
    }
}
