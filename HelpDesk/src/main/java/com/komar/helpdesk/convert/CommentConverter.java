package com.komar.helpdesk.convert;

import com.komar.helpdesk.dto.CommentDto;
import com.komar.helpdesk.repository.model.Comment;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

/**
 *
 * @author Маріна
 */

@Component
public class CommentConverter {
    public CommentDto toDto(Comment comment){
        return CommentDto.builder()
                .date(comment.getDate())
                .user(comment.getUser().getFirstName() + " " + comment.getUser().getLastName())
                .text(comment.getText())
                .build();
    }
    
    public List<CommentDto> toDtoList(List<Comment> list){
        return list.stream()
                .map(comment -> toDto(comment))
                .collect(Collectors.toList());
    }
}
