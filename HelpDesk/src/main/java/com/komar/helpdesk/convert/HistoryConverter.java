package com.komar.helpdesk.convert;

import com.komar.helpdesk.dto.HistoryDto;
import com.komar.helpdesk.repository.model.History;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

/**
 *
 * @author Маріна
 */

@Component
public class HistoryConverter {
    
    public HistoryDto toDto(History history){
        return HistoryDto.builder()
                .user(history.getUser().getFirstName() + " " + history.getUser().getLastName())
                .date(history.getDate())
                .action(history.getAction())
                .description(history.getDescription())
                .build();
    }
    
    public List<HistoryDto> toDtoList(List<History> list){
        return list.stream()
                .map(history -> toDto(history))
                .collect(Collectors.toList());
    }
}
