package com.komar.helpdesk.convert;

import com.komar.helpdesk.dto.TicketOverviewDto;
import com.komar.helpdesk.repository.model.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Маріна
 */

@Component
public class TicketOverviewConverter {   
    @Autowired
    private AttachmentConverter attachmentConverter;
    
    @Autowired
    private HistoryConverter hictoryConverter;
    
    @Autowired
    private CommentConverter commentConverter;
    
    public TicketOverviewDto toOverviewDto(Ticket ticket){
        return TicketOverviewDto.builder()
                .category((ticket.getCategory() == null) ? "" : ticket.getCategory().getName())
                .name(ticket.getName())
                .description(ticket.getDescription())
                .urgency((ticket.getUrgency() == null) ? "" : ticket.getUrgency().getUrgency())
                .status(ticket.getStatus().getState())
                .desiredDate(ticket.getDesiredDate())
                .createdOn(ticket.getCreatedOn())
                .attachments(attachmentConverter.toPreviewDtoList(ticket.getAttachments()))
                .approver((ticket.getApprover() != null ) ?
                        (ticket.getApprover().getFirstName() + " " + ticket.getApprover().getLastName()) :
                        "none")
                .owner(ticket.getOwner().getFirstName() + " " + ticket.getOwner().getLastName())
                .assignee((ticket.getAssignee()!= null ) ?
                        (ticket.getAssignee().getFirstName() + " " + ticket.getAssignee().getLastName()) :
                        "none")
                .history(hictoryConverter.toDtoList(ticket.getHistory().subList(0, (4 > ticket.getHistory().size()) ? ticket.getHistory().size() : 4)))
                .comments(commentConverter.toDtoList(ticket.getComments().subList(0, (4 > ticket.getComments().size()) ? ticket.getComments().size() : 4)))
                .build();
                
    }

}
