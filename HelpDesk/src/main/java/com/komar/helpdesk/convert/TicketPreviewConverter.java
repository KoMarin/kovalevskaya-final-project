package com.komar.helpdesk.convert;

import com.komar.helpdesk.enums.Actions;
import com.komar.helpdesk.dto.TicketPreviewDto;
import com.komar.helpdesk.repository.model.Ticket;
import com.komar.helpdesk.repository.model.User;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

/**
 *
 * @author Маріна
 */
@Component
public class TicketPreviewConverter {
    
     public TicketPreviewDto toPreviewDto(Ticket ticket, User user){
         return TicketPreviewDto.builder()
                .id(ticket.getId())
                .name(ticket.getName())
                .desiredDate(ticket.getDesiredDate())
                .urgency(ticket.getUrgency())
                .status(ticket.getStatus().getState())
                .possibleActions(Actions.allowedActions(user, ticket))
                .build();
                
    }
     
     public List<TicketPreviewDto> toPreviewDtotList(Collection<Ticket> tickets, User user){
         return tickets.stream()
                .map(ticket -> toPreviewDto(ticket, user))
                .collect(Collectors.toList());
     }

}
