package com.komar.helpdesk.convert;

import com.komar.helpdesk.dto.TicketForEditDto;
import com.komar.helpdesk.enums.States;
import com.komar.helpdesk.repository.model.Ticket;
import com.komar.helpdesk.service.CategoryService;
import com.komar.helpdesk.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Маріна
 */

@Component
public class TicketEditConverter {
    
    @Autowired
    private CategoryService categoryService;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private AttachmentConverter attachmentConverter;

    public TicketEditConverter() {
    }

    public TicketEditConverter(CategoryService categoryService, UserService userService, AttachmentConverter attachmentConverter) {
        this.categoryService = categoryService;
        this.userService = userService;
        this.attachmentConverter = attachmentConverter;
    }
    
    public TicketForEditDto toEditDto(Ticket ticket){
        return TicketForEditDto.builder()
                .id(ticket.getId())
                .category((ticket.getCategory() == null) ? 0 : ticket.getCategory().getId())
                .name(ticket.getName())
                .status(ticket.getStatus().name())
                .description(ticket.getDescription())
                .urgency(ticket.getUrgency())
                .desiredDate(ticket.getDesiredDate())
                .savedAttachments(attachmentConverter.toPreviewDtoList(ticket.getAttachments()))
                .owner(ticket.getOwner().getEmail())
                .build();
                
    }
    
    public Ticket toEntity(TicketForEditDto ticket){
        return Ticket.builder()
                .id(ticket.getId())
                .category(categoryService.getCategory(ticket.getCategory()))
                .name(ticket.getName())
                .description(ticket.getDescription())
                .urgency(ticket.getUrgency())
                .desiredDate(ticket.getDesiredDate())
                .status(States.valueOf(ticket.getStatus()))
                .owner(userService.getUser(ticket.getOwner()))
                .attachments(attachmentConverter.toEntityList(ticket.getSavedAttachments()))///!!!!!!!! yes !!
                .build(); 
    }
    
}
